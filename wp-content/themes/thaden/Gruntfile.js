// Gruntfile.js

// our wrapper function (required by grunt and its plugins)
// all configuration goes inside this function
module.exports = function (grunt) {
    // ===========================================================================
    // CONFIGURE GRUNT ===========================================================
    // ===========================================================================
    grunt.initConfig({
        // get the configuration info from package.json ----------------------------
        // this way we can use things like name and version (pkg.name)
        pkg: grunt.file.readJSON('package.json'),
        // all of our configuration will go here
        // configure jshint to validate js files -----------------------------------
        jshint: {
            options: {
                reporter: require('jshint-stylish') // use jshint-stylish to make our errors look and read good
            },
            // when this task is run, lint the Gruntfile and all js files in src
            build: ['Gruntfile.js', 'src/js/magic.js']
        },
        // compile Js ------------------------------------------------------------
        uglify: {
            my_target: {
                files: {
                    'dist/js/jquery.min.js':
                            [
                                'src/js/jquery.js'
                            ],
                    'dist/js/html5shiv.respond.min.js':
                            [
                                'src/js/html5shiv.js',
                                'src/js/respond.js'
                            ],
                    'dist/js/plugins.min.js':
                            [
                                'src/js/lib/classie.js',
                                'src/js/lib/fixedto.js',
                                'src/js/lib/jquery.fancybox.js',
                                'src/js/lib/flickity.pkgd.js',
                                'src/js/lib/flickity-fade.js',
                                'src/js/lib/flickity-imagesloaded.js',
                                'src/js/lib/jquery.mCustomScrollbar.js',
                                'src/js/lib/slick.js',
                                'src/js/lib/jquery.selectric.js',
                                'src/js/lib/filter.js'
                            ],
                    'dist/js/index.min.js':
                            [
                                'src/js/index.js'
                            ]
                }
            }
        },
        // compile less stylesheets to css -----------------------------------------
        less: {
            development: {
                files: {
                    'dist/css/thaden-style.css': 'src/less/thaden-style.less'
                },
                options: {
                    compress: true,
                    cleancss: true,
                    optimization: 2,
                    sourceMap: true,
                    sourceMapFilename: "dist/css/thaden-style.css.map",
                    sourceMapBasepath: "dist/css/"
                }
            }
        },
        // optimize images -----------------------------------------
        imagemin: {
            dynamic: {
                options: {
                    progressive: true
                },
                files: [{
                        expand: true,
                        cwd: 'src/images',
                        src: ['**/*.{png,jpg,gif}'],
                        dest: 'dist/images/'
                    }]
            }
        },
        // configure watch to auto update ----------------
        watch: {
            design: {
                files: ['dist/**/.css', 'src/**/*.less'],
                tasks: ['less'],
                options: {
                    nospawn: true
                }
            },
            // for scripts, run jshint and uglify 
            scripts: {
                files: 'src/**/*.js', tasks: ['jshint', 'uglify']
            }
        }
    });

    // ===========================================================================
    // LOAD GRUNT PLUGINS ========================================================
    // ===========================================================================
    // we can only load these if they are in our package.json
    // make sure you have run npm install so our app can find these
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.registerTask('default', ['less', 'watch', 'uglify', 'imagemin']);
};
