<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage thaden
 * @since thaden 1.0
 */
get_header('nogap');
?>

<section class="fl-fix pos-r cms-pages">
    <div class="full-wrapper">
        <?php
        while (have_posts()) :
            the_post();
            get_template_part('template-parts/content/content', 'page');

            // If comments are open or we have at least one comment, load up the comment template.
            if (comments_open() || get_comments_number()) {
                comments_template();
            }
        endwhile; // End the loop.
        ?>
    </div><!-- #primary -->
</section>	

<?php
get_footer();
