<?php
/**
 * Displays header site branding
 *
 * @package WordPress
 * @subpackage thaden
 * @since thaden 1.0
 */
?>

<header class="header-page <?php
if (!is_front_page()) {
    echo 'no-gap';
}
?>">
    <div class="full-wrapper">
        <div class="header-navbar d-f ai-c pos-r jc-c">
            <a class="nav-toggle pos-a"> <span></span> </a>
            <!-- Menu Button -->
            <div class="logo-box">
                <a href="<?php echo home_url('/'); ?>" class="logo d-ib va-t"><img src="<?php echo get_option('logo'); ?>" alt="Thaden Logo" width="376" height="100"></a>
            </div>
            <!-- Logo -->
            <div class="header-navbar-right pos-a">
                <ul class="right-panel d-f ai-c">
                    <li class="account icon pos-r">
                        <?php if (is_user_logged_in()) { ?>
                            <a class="d-b" href="<?php echo home_url('my-account/'); ?>"></a>
                            <!--div class="profileNav">
                                    <ul>
                            <?php foreach (wc_get_account_menu_items() as $endpoint => $label) : ?>
                                                                            <li class="<?php echo wc_get_account_menu_item_classes($endpoint); ?>">
                                                                                    <a href="<?php echo esc_url(wc_get_account_endpoint_url($endpoint)); ?>" <?php if ($label == 'SIGN OUT'): ?> class="btn"<?php endif; ?>><?php echo esc_html($label); ?></a>
                                                                            </li>
                            <?php endforeach; ?>
                                    </ul>
                            </div-->
                        <?php } else { ?>
                            <a class="d-b" href="<?php echo home_url('my-account/'); ?>"></a>
                        <?php } ?>						
                    </li>
                    <li class="currency icon pos-r"> <?php echo do_shortcode('[woo_multi_currency_layout10]'); ?> </li>
                    <?php echo do_shortcode("[woo_cart_but]"); ?>

                </ul>
            </div>
        </div>

        <div class="menu-main">
            <div class="full-wrapper">
                <div class="menu-main-wrap d-f fxw-w">
                    <div class="logo-box">
                        <a href="<?php echo home_url('/'); ?>" class="logo d-ib va-t"><img src="<?php echo get_option('logo'); ?>" alt="Thaden Logo" width="376" height="100"></a>
                    </div>
                    <div class="menu-main-nav d-f w100 tt-u c-black">
                        <?php if (is_active_sidebar('navigation-sidebar-1')) { ?>
                            <div class="col">
                                <?php echo dynamic_sidebar('navigation-sidebar-1'); ?>
                            </div>
                        <?php } ?>	
                        <?php if (is_active_sidebar('navigation-sidebar-2')) { ?>
                            <div class="col">
                                <?php dynamic_sidebar('navigation-sidebar-2'); ?>
                            </div>
                        <?php } ?>
                        <?php if (is_active_sidebar('navigation-sidebar-3')) { ?>
                            <div class="col">
                                <?php dynamic_sidebar('navigation-sidebar-3'); ?>
                            </div>
                        <?php } ?>					 
                    </div>
                </div>
            </div> 
        </div>
    </div>
</header>

