<?php
$herobanners = get_field('hero_banner');
if (!empty($herobanners)):
    ?>
    <section class="fl-fix pos-r inner-banner ta-l">
        <figure class="figure">
            <img src="<?php echo $herobanners['url']; ?>" alt="banner-about-thaden" width="1920" height="868" class="w100">
        </figure>
        <div class="caption pos-a d-f ai-c">
            <div class="full-wrapper">
                <div class="wrapper">
                    <?php if (!empty(get_field('banner_heading'))): ?>
                        <h1 class="h1 title tt-u c-black"><?php echo get_field('banner_heading'); ?></h1>
                    <?php else: ?>
                        <h1 class="h1 title tt-u c-black"><?php echo get_the_title(); ?></h1>
                    <?php endif; ?>				
                    <?php echo apply_filters('the_Content', get_field('banner_content')); ?>
                </div>
            </div>
        </div>
    </section>

<?php endif; ?>
<?php if (is_shop() || is_product_category()): ?>
    <?php $banners = get_field('hero_banner', get_option('woocommerce_shop_page_id')); ?>
    <section class="fl-fix pos-r inner-banner ta-l">
        <figure class="figure">
            <img src="<?php echo $banners['url']; ?>" alt="<?php echo $banners['alt']; ?>" width="1920" height="868" class="w100">
        </figure>
        <div class="caption pos-a d-f ai-c">
            <div class="full-wrapper">
                <div class="wrapper">
                    <?php if (!empty(get_field('banner_heading', get_option('woocommerce_shop_page_id')))): ?>
                        <h1 class="h1 title tt-u c-black"><?php echo get_field('banner_heading', get_option('woocommerce_shop_page_id')); ?></h1>
                    <?php else: ?>
                        <h1 class="h1 title tt-u c-black"><?php echo get_the_title(); ?></h1>
                    <?php endif; ?>				
                    <?php echo apply_filters('the_Content', get_field('banner_content', get_option('woocommerce_shop_page_id'))); ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
<!-- Home Banner Section -->