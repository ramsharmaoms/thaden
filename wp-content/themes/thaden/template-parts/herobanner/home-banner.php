<?php
$bannersliders = get_field('sliders');
if (!empty($bannersliders)) {
    ?>
    <section class="fl-fix pos-r home-banner">
        <div class="full-wrapper">
            <div class="home-slider fl-fix">
                <?php
                foreach ($bannersliders as $bannerslider) {
                    $sliderImage = $bannerslider['slider_images'];
                    $sliderTitle = $bannerslider['slider_title'];
                    $sliderContent = $bannerslider['slider_content'];
                    $sliderButton = $bannerslider['slider_button_name'];
                    $sliderButtonurl = $bannerslider['slider_button_url'];
                    if (!empty($sliderImage)) {
                        ?>		
                        <div class="home-slide fl-fix ta-l">
                            <figure class="bgsz-cv bgp-cc bgr-n figure" style="background-image: url('<?php echo $sliderImage['url']; ?>');">
                                <img src="<?php echo get_template_directory_uri(); ?>/dist/images/home-banner-1.gif" alt="Placeholder" width="100" height="67" class="w100">
                            </figure>
                            <div class="caption pos-a d-f ai-c">
                                <div class="wrapper">
                                    <?php if (!empty($sliderTitle)) { ?>	<h1 class="h1 title tt-u c-black"><?php echo $sliderTitle; ?></h1><?php } ?>
            <?php if (!empty($sliderContent)) {
                echo $sliderContent;
            } ?>
                        <?php if (!empty($sliderButton)) { ?><a href="<?php echo $sliderButtonurl; ?>" class="btn"><?php echo $sliderButton; ?></a><?php } ?>
                                </div>
                            </div>
                        </div>
        <?php }
    } ?>	
            </div>
        </div>
    </section>
<?php } ?>	
<!-- Home Banner Section-->