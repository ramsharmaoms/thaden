<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage thaden
 * @since thaden 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php if (!thaden_can_show_post_thumbnail()) : ?>
        <section class="fl-fix pos-r page-title ta-c">
            <?php get_template_part('template-parts/header/entry', 'header'); ?>
        </section>
    <?php endif; ?>
    <section class="fl-fix pos-r page-inner">
        <div class="inner-wrapper">
            <?php
            the_content();
            ?>
        </div>
    </section>
</article><!-- #post-<?php the_ID(); ?> -->
