<?php
/*
 * * get product categories
 * *
 */

function get_woocommerce_category() {
    $taxonomy = 'product_cat';
    $orderby = 'name';
    $show_count = 0;      // 1 for yes, 0 for no
    $pad_counts = 0;      // 1 for yes, 0 for no
    $hierarchical = 1;      // 1 for yes, 0 for no  
    $title = '';
    $empty = 0;

    $args = array(
        'taxonomy' => $taxonomy,
        'orderby' => $orderby,
        'show_count' => $show_count,
        'pad_counts' => $pad_counts,
        'hierarchical' => $hierarchical,
        'title_li' => $title,
        'hide_empty' => $empty
    );
    $all_categories = get_categories($args);
    return $all_categories;
    exit;
}

/**
 * * redirect shop page to flavors page
 */
//add_filter( 'woocommerce_return_to_shop_redirect', "custom_woocommerce_return_to_shop_redirect" ,20 );
function custom_woocommerce_return_to_shop_redirect() {
    return site_url() . "/shop";
}

/*
 * * top menu cart icon
 * * create shortcode
 */

add_shortcode('woo_cart_but', 'woo_cart_but');

/**
 * Create Shortcode for WooCommerce Cart Menu Item
 */
function woo_cart_but() {
    ob_start();

    $cart_count = WC()->cart->cart_contents_count; // Set variable for cart item count
    $cart_url = wc_get_cart_url();  // Set Cart URL
    ?>
    <li class="nav-cart icon pos-r"> <a class="d-b" href="<?php echo $cart_url; ?>">
            <?php
            if ($cart_count > 0) {
                ?>
                <span class="nav-cart-count d-f ai-c jc-c cart-contents-count"><?php echo $cart_count; ?></span>
                <?php
            }
            ?>
        </a>
        <div class="minicart">
            <div class="minicart-inner">
                <p class="woocommerce-mini-cart__empty-message"><?php esc_html_e('No products in the cart.', 'woocommerce'); ?></p>
            </div>
        </div>
    </li>
    <?php
    return ob_get_clean();
}

/**
 * Cart widget
 */
if (!function_exists('yogesh_woocommerce_widget_shopping_cart_subtotal')) {

    /**
     * Output to view cart subtotal.
     *
     * @since 3.7.0
     */
    function yogesh_woocommerce_widget_shopping_cart_subtotal() {
        echo '<div class="sub-total">' . esc_html__('Subtotal', 'woocommerce') . ' ' . WC()->cart->get_cart_subtotal() . '</div>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
        echo '<div class="notes">*Shipping charges are applicable</div>';
    }

}


if (!function_exists('yogesh_woocommerce_widget_shopping_cart_button_view_cart')) {

    /**
     * Output the view cart button.
     */
    function yogesh_woocommerce_widget_shopping_cart_button_view_cart() {
        echo '<a href="' . esc_url(wc_get_cart_url()) . '" class=" btn wc-forward">' . esc_html__('View cart', 'woocommerce') . '</a>';
    }

}

if (!function_exists('yogesh_woocommerce_widget_shopping_cart_proceed_to_checkout')) {

    /**
     * Output the proceed to checkout button.
     */
    function yogesh_woocommerce_widget_shopping_cart_proceed_to_checkout() {
        echo '<a href="' . esc_url(wc_get_checkout_url()) . '" class=" btn checkout wc-forward">' . esc_html__('Checkout', 'woocommerce') . '</a>';
    }

}


add_filter('woocommerce_add_to_cart_fragments', 'woo_cart_but_count');

/**
 * Add AJAX Shortcode when cart contents update
 */
function woo_cart_but_count($fragments) {

    ob_start();

    $cart_count = WC()->cart->cart_contents_count;
    $cart_url = wc_get_cart_url();
    ?>
    <a class="d-b cart-total-header" href="<?php echo $cart_url; ?>">
        <?php
        if ($cart_count > 0) {
            ?>
            <span class="nav-cart-count d-f ai-c jc-c cart-contents-count"><?php echo $cart_count; ?></span>
            <?php
        }
        ?></a>
    <?php
    $fragments['.cart-total-header'] = ob_get_clean();

    return $fragments;
}

/**
 * ** Woocommerce funciton for listing page
 * * chnage woocommerce hooks and add prefix as yogesh for all
 * *
 */
//Define number of products per page
add_filter('loop_shop_per_page', 'yogesh_woocommerce_products_per_page', 20);

//Set number of related products
add_filter('woocommerce_output_related_products_args', 'yogesh_woocommerce_related_products_args');
if (!function_exists('yogesh_woocommerce_products_per_page')) {

    /**
     * Function that sets number of products per page. Default is 12
     * @return int number of products to be shown per page
     */
    function yogesh_woocommerce_products_per_page() {
        $products_per_page = 2;
        return $products_per_page;
    }

}

if (!function_exists('yogesh_woocommerce_related_products_args')) {

    /**
     * Function that sets number of displayed related products. Hooks to woocommerce_output_related_products_args filter
     * @param $args array array of args for the query
     * @return mixed array of changed args
     */
    function yogesh_woocommerce_related_products_args($args) {
        $args['posts_per_page'] = 3;
        return $args;
    }

}
remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_orderby', 30);
remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
add_action('yogesh_woocommerce_catalog_orderby', 'yogesh_override_woocommerce_catalog_orderby', 10);
//Override porduct list order by select

if (!function_exists('yogesh_override_woocommerce_catalog_orderby')) {

    function yogesh_override_woocommerce_catalog_orderby() {

        $args = apply_filters('woocommerce_catalog_orderby', array(
            'menu_order' => esc_html__('Default', 'thaden'),
            'popularity' => esc_html__('Popularity', 'thaden'),
            'rating' => esc_html__('Average Rating', 'thaden'),
            'date' => esc_html__('Newness', 'thaden'),
            'price' => esc_html__('Price: Low to High', 'thaden'),
            'price-desc' => esc_html__('Price: High to Low', 'thaden')
        ));

        return $args;
    }

}

function mytheme_add_woocommerce_support() {
    add_theme_support('woocommerce');
}

add_action('after_setup_theme', 'mytheme_add_woocommerce_support');

add_filter('woocommerce_add_to_cart_fragments', function ($fragments) {

    ob_start();
    ?>

    <div class="cart-contents">
        <?php echo WC()->cart->get_cart_contents_count(); ?>
    </div>

    <?php
    $fragments['div.cart-contents'] = ob_get_clean();

    return $fragments;
});

add_filter('woocommerce_add_to_cart_fragments', function ($fragments) {

    ob_start();
    ?>

    <div class="minicart-inner">
        <?php echo wc_get_template('cart/mini-cart.php'); ?>
    </div>

    <?php
    $fragments['div.minicart-inner'] = ob_get_clean();

    return $fragments;
});

add_action('yogesh_woocommerce_before_shop_loop_item_title', 'yogesh_woocommerce_template_loop_product_thumbnail', 10);
remove_action('yogesh_woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10);
remove_action('woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10);

function yogesh_woocommerce_template_loop_product_thumbnail() {
    global $product;
    $product_featured_image_id = get_post_thumbnail_id($product->id);
    $alt = get_post_meta($product_featured_image_id, '_wp_attachment_image_alt', true);
    $attachment = wp_get_attachment_image_src($product_featured_image_id, 'full');
    echo '<figure class="figure pos-r w100"> <a href="' . get_permalink() . '" class="product-link pos-a d-f ai-c jc-c">';
    echo '<img src="' . $attachment[0] . '" alt="' . $alt . '" width="495" height="618" class="product-image" />';
    echo '</a></figure>';
}

add_action('yogesh_woocommerce_shop_loop_item_title', 'yogesh_woocommerce_template_loop_product_title', 10);

function yogesh_woocommerce_template_loop_product_title() {
    global $product;
      $color = $product->get_attribute( 'pa_color' );
    echo '<h4 class="product-name h4"><a href="' . get_permalink() . '">' . get_the_title() . '</a></h4>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
       echo '<h6 class="product-color-list marB10">'.$color.'</h6>';
    
}

add_action('yogesh_woocommerce_after_shop_loop_item_title', 'yogesh_woocommerce_template_loop_price', 10);

function yogesh_woocommerce_template_loop_price() {
    global $product;  		
    echo '<div class="product-price fs6">' . $product->get_price_html() . '</div>';
 
}

remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 10);
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
///// end product listing page ////
/// Single product ////

remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
add_action('woocommerce_before_main_content', 'omnie_back_button_wrapper', 30);

function omnie_back_button_wrapper() {
    global $product;
    $product_cats = wp_get_post_terms(get_the_ID(), 'product_cat');
    ?>
    <div class="back-page fl-fix pos-r d-f">
        <div class="full-wrapper">
            <a href="<?php echo get_term_link($product_cats[0]->slug, 'product_cat'); ?>" class="btn-back d-ib va-t pos-r tt-u c-black">Back</a>
        </div>
    </div>
    <?php
}

remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20);
add_action('woocommerce_before_single_product_summary', 'omnie_woocommerce_show_product_images', 20);

function omnie_woocommerce_show_product_images() {
    global $product;
    $product_featured_image_id = get_post_thumbnail_id($product->id);
    $attachment_ids = $product->get_gallery_attachment_ids();
    $attachment = wp_get_attachment_image_src($product_featured_image_id, 'thumb');
    $alt = get_post_meta($product_featured_image_id, '_wp_attachment_image_alt', true);
    $columns = apply_filters('woocommerce_product_thumbnails_columns', 4);
    $post_thumbnail_id = $product->get_image_id();
    $wrapper_classes = apply_filters(
            'woocommerce_single_product_image_gallery_classes',
            array(
                'woocommerce-product-gallery',
                'woocommerce-product-gallery--' . ( $post_thumbnail_id ? 'with-images' : 'without-images' ),
                'woocommerce-product-gallery--columns-' . absint($columns),
                'images',
            )
    );
    ?>
    <?php
    echo '<div class="product-slider-image col w55 padR50">';
    if (!empty($attachment_ids)):
        echo '<div class="sticky-thumb">
					<div class="product-carousel-nav">
						<div class="carousel-cell">
							<figure>
								<img src="' . $attachment[0] . '" alt="' . $alt . '" width="80" height="80">
							</figure>
						</div>';
        foreach ($attachment_ids as $attachment_id):
            $attachmentthumb = wp_get_attachment_image_src($attachment_id, 'thumb');
            $altthumb = get_post_meta($product_featured_image_id, '_wp_attachment_image_alt', true);
            echo '<div class="carousel-cell">
								<figure>
									<img src="' . $attachmentthumb[0] . '" alt="' . $altthumb . '" width="80" height="80">
								</figure>
							</div>';
        endforeach;
        echo '</div>
				</div>';
    endif;
    echo '<div class="product-carousel-main">';
    $attachmentmainthumb = wp_get_attachment_image_src($product_featured_image_id, 'full');
    echo '<div class="carousel-cell">
					<figure>
                                            <a href="' . $attachmentmainthumb[0] . '" data-fancybox="product-gallery">
						<img src="' . $attachmentmainthumb[0] . '" alt="' . $alt . '" width="740" height="740">
                                            </a>
					</figure>
				</div>';
    foreach ($attachment_ids as $attachment_id):
        $attachmentthumb = wp_get_attachment_image_src($attachment_id, 'full');
        $altthumb = get_post_meta($product_featured_image_id, '_wp_attachment_image_alt', true);
        echo '<div class="carousel-cell">
						<figure>
                                                    <a href="' . $attachmentthumb[0] . '" data-fancybox="product-gallery">
							<img src="' . $attachmentthumb[0] . '" alt="' . $altthumb . '" width="740" height="740">
                                                    </a>
						</figure>
					</div>';
    endforeach;
    echo '</div>';
    echo '</div>';
}

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
//add_action('woocommerce_single_product_summary','woocommerce_template_single_price',40);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 10);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
add_action('woocommerce_after_add_to_cart_quantity', 'ts_quantity_plus_sign');

function ts_quantity_plus_sign() {
    echo '<span class="plus btn-qty">+</span>';
}

add_action('woocommerce_before_add_to_cart_quantity', 'ts_quantity_minus_sign');

function ts_quantity_minus_sign() {
    echo ' <span class="minus btn-qty">-</span>';
}

add_action('wp_footer', 'ts_quantity_plus_minus');

function ts_quantity_plus_minus() {
    // To run this on the single product page
    if (!is_product())
        return;
    ?>
    <script type="text/javascript">

        jQuery(document).ready(function ($) {

            $('form.cart').on('click', 'span.plus, span.minus', function () {

                // Get current quantity values
                var qty = $(this).closest('form.cart').find('.qty');
                var val = parseFloat(qty.val());
                var max = parseFloat(qty.attr('max'));
                var min = parseFloat(qty.attr('min'));
                var step = parseFloat(qty.attr('step'));

                // Change the value if plus or minus
                if ($(this).is('.plus')) {
                    if (max && (max <= val)) {
                        qty.val(max);
                    } else {
                        qty.val(val + step);
                    }
                } else {
                    if (min && (min >= val)) {
                        qty.val(min);
                    } else if (val > 1) {
                        qty.val(val - step);
                    }
                }

            });

        });

    </script>
    <?php
}
