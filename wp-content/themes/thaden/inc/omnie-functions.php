<?php

/* Better way to add multiple widgets areas */

function widget_registration($name, $id, $description, $beforeWidget, $afterWidget, $beforeTitle, $afterTitle) {
    register_sidebar(array(
        'name' => $name,
        'id' => $id,
        'description' => $description,
        'before_widget' => $beforeWidget,
        'after_widget' => $afterWidget,
        'before_title' => $beforeTitle,
        'after_title' => $afterTitle,
    ));
}

function multiple_widget_init() {
    widget_registration('Navigation for Category 1', 'navigation-sidebar-1', 'Add sidebar for navigation to add category with images', '', '', '', '');
    widget_registration('Navigation for Category 2', 'navigation-sidebar-2', 'Add sidebar for navigation to add category with images', '', '', '', '');
    widget_registration('Navigation for Pages', 'navigation-sidebar-3', 'Add sidebar for navigation to add pages links', '', '', '', '');
    widget_registration('Instagram Feed', 'instagram-feed', 'Add sidebar for Instagram Feed', '', '', '', '');
    // ETC...
}

add_action('widgets_init', 'multiple_widget_init');

add_action('wp_ajax_get_thaden_faqs', 'get_thaden_faqs'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_get_thaden_faqs', 'get_thaden_faqs'); // wp_ajax_nopriv_{action}

function get_thaden_faqs() {
    $faqcatid = $_POST['category'];
    $args = array('post_type' => 'faqs', 'post_status' => 'publish', 'posts_per_page' => -1, 'tax_query' => array(
            array(
                'taxonomy' => 'thaden_faq',
                'field' => 'term_id',
                'terms' => $faqcatid,
            ),));
    $html = '';
    $query = new WP_Query($args);
    $i = 0;
    if ($query->have_posts()):
        while ($query->have_posts()) : $query->the_post();
            $html .= '<div class="faq-row">
            <h5 class="h5 title pos-r">' . get_the_title() . '</h5>
            <div class="faq-text">
            ' . apply_filters('the_content', $query->post->post_content) . '
            </div>
        </div>';
        endwhile;
    endif;
    wp_reset_query();
    echo $html;
    exit;
}
