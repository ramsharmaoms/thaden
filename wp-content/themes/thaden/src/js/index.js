(function () {
    if ($(".header-page").length > 0) {
        var docElem = document.documentElement,
                $html = document.querySelector("html"),
                didScroll = false,
                changeHeaderOn = 10,
                changeHeaderOnNew = 25,
                secondSectionOn = false;

        function init() {
            window.addEventListener(
                    "scroll",
                    function (event) {
                        if (!didScroll) {
                            didScroll = true;
                            setTimeout(scrollPage, 5);
                        }
                    },
                    false
                    );
        }

        scrollPage();
        if ($(window).scrollTop() < 5) {
            secondSectionOn = true;
        } else {
            secondSectionOn = false;
        }

        function scrollPage() {
            if ($('.header-page').length) {

                var sy = scrollY();
                if (sy >= changeHeaderOn) {

                    if (secondSectionOn) {
                        var hHeight = $('.header-page').outerHeight(true);
                        if (window.innerWidth > 1024) {
                            if (!$('html').hasClass('btn-clicked')) {

                            }
                        }

                        secondSectionOn = false;
                    }

                } else {
                    if ($(window).scrollTop() < 5) {
                        secondSectionOn = true;
                    }
                }

                if (sy >= changeHeaderOnNew) {
                    if (!$('html').hasClass('animating')) {
                        classie.add($html, "compressed");
                    }
                } else {
                    classie.remove($html, "compressed");
                }


                didScroll = false;

            }
        }


        function scrollY() {
            return window.pageYOffset || docElem.scrollTop;
        }
        init();
    }



})();

$(function () {
    if (-1 != navigator.userAgent.indexOf("MSIE"))
        var detectIEregexp = /MSIE (\d+\.\d+);/;
    else
        var detectIEregexp = /Trident.*rv[ :]*(\d+\.\d+)/;
    if (detectIEregexp.test(navigator.userAgent)) {
        var ieversion = new Number(RegExp.$1);
        12 == ieversion ?
                jQuery("body").addClass("IE12") :
                11 == ieversion ?
                jQuery("body").addClass("IE11") :
                10 == ieversion ?
                jQuery("body").addClass("IE10") :
                9 == ieversion && jQuery("body").addClass("IE9");
    }
    //Search for keywords within the user agent string.  When a match is found, add a corresponding class to the html element and return.  (Inspired by http://stackoverflow.com/a/10137912/114558)
    function addUserAgentClass(keywords) {
        for (var i = 0; i < keywords.length; i++) {
            if (navigator.userAgent.indexOf(keywords[i]) != -1) {
                $("html").addClass(keywords[i].toLowerCase());
                return; //Once we find and process a matching keyword, return to prevent less "specific" classes from being added
            }
        }
    }
    addUserAgentClass([
        "Chrome",
        "Firefox",
        "MSIE",
        "Safari",
        "Opera",
        "Mozilla"
    ]); //Browsers listed generally from most-specific to least-specific
    addUserAgentClass(["Android", "iPhone", "iPad", "Linux", "Mac", "Windows"]); //Platforms, also in order of specificity

    (function () {
        // Really basic check for the ios platform
        // https://stackoverflow.com/questions/9038625/detect-if-device-is-ios
        var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
        // Get the device pixel ratio
        var ratio = window.devicePixelRatio || 1;
        // Define the users device screen dimensions
        var screen = {
            width: window.screen.width * ratio,
            height: window.screen.height * ratio
        };
        // iPhone X Detection
        if (iOS && screen.width == 1125 && screen.height === 2436) {
            $("html").addClass("iphoneX");
        }
    })();


    $(".tab-list .item").click(function () {
        var curIndex = $(this).index();
        if ($(this).parents().hasClass('tab-content')) {
            $('.jpPosition2 a').eq(curIndex).click();

        } else {
            $(".tab-list .item").removeClass("active");
            $(".tab-content-box .tab-content").hide();
            $(this).addClass("active");
            $(".tab-content-box .tab-content")
                    .eq(curIndex)
                    .show();
        }
        return false;
    });


    $(".filter-grid .view-link").click(function () {
        var curView = $(this).data('view');
        $(".filter-grid li").removeClass("active");
        $(this).parent().addClass("active");
        $('.product-list-box').removeClass('grid-view-2 grid-view-3');
        $('.product-list-box').addClass(curView);
        return false;
    });

    $(".filter-btn li:first .btn, .tab-list .item:first").trigger('click');


    // nav //
    $(".nav-toggle").click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (!$("html").hasClass("menu-open")) {
            $(".nav-toggle").addClass("active");
            $("html").addClass("menu-open");

        } else {
            $(".nav-toggle").removeClass("active");
            $("html").removeClass("menu-open");
        }
    });





    $('.c-select select').selectric({});
    $('.gfield select').selectric({});


//    if ($("#newsletter-popup").length) {
//        setTimeout(function () {
//            $("#newsletter-popup").fancybox({
//                clickContent: true,
//                clickSlide: true,
//                clickOutside: true,
//                dblclickContent: true,
//                dblclickSlide: true,
//                dblclickOutside: true,
//                hideOnContentClick : true
////                afterClose: function () {
////                    if (typeof (Storage) !== "undefined") {
////                        sessionStorage.setItem("modalClose", true);
////                    }
////                }
//            }).trigger('click');
//        }, 1000);
//    }

    setTimeout(function () {
        $(".full-page-loader").addClass("start anim");
    }, 30);
    setTimeout(function () {
        $(".full-page-loader svg path").addClass("anim");
    }, 300);
    setTimeout(function () {
        $(".full-page-loader").addClass("end");
    }, 400);


    var resizeId;
    $(window).resize(function () {
        clearTimeout(resizeId);
        resizeId = setTimeout(doneResizing, 250);
    });
    $(window).bind("orientationchange", function () {
        doneResizing();
    });

    $('.home-slider').flickity({
        cellAlign: 'left',
        contain: true,
        wrapAround: true,
        autoPlay: 6000,
        //autoPlay: false,
        fade: true,
        pauseAutoPlayOnHover: false,
        prevNextButtons: false,
        imagesLoaded: true,
        pageDots: true,
        on: {
            ready: function () {

            },
            change: function (index) {

            }
        }
    });

    $('.moveDown').on('click', function () {
        //var scrollPos = $('.banner-tabs').offset().top;
        var scrollPos = $(this).parent().next().offset().top;
        $('html, body').animate({
            scrollTop: scrollPos
        }, 1000);
        return false;
    });


    $(".product-carousel-main").flickity({
        contain: true,
        wrapAround: false,
        autoPlay: false,
        fade: false,
        groupCells: '100%',
        prevNextButtons: true,
        percentPosition: false,
        imagesLoaded: true,
        pageDots: true,
        adaptiveHeight: true
    });



    $(".product-carousel-nav").flickity({
        asNavFor: ".product-carousel-main",
        contain: true,
        wrapAround: false,
        autoPlay: false,
        fade: false,
        draggable: false,
        percentPosition: false,
        groupCells: '100%',
        prevNextButtons: true,
        imagesLoaded: true,
        pageDots: false
    });

    $(".product-carousel-nav .carousel-cell").click(function () {
        var curIndex = $(this).index();
        var hHeight = $('.header-page').outerHeight(true);
        var bHeight = $('.back-page').outerHeight(true);
        $('html, body').animate({
            scrollTop: $('.product-carousel-main .carousel-cell:eq(' + curIndex + ')').offset().top - hHeight - bHeight
        }, 500);
    });

    $('.sticky-box').fixTo('.product-info', {
        mind: '.header-page',
        top: 0
    });

    $('.sticky-thumb').fixTo('.product-box', {
        top: 0
    });

    $('.back-page').fixTo('.product-box', {
        top: 0
    });

//    $('.product-carousel-main').slick({
//        slidesToShow: 1,
//        vertical: true,
//        slidesToScroll: 1,
//        arrows: true,
//        infinite: false,
//        asNavFor: '.product-carousel-nav'
//    });
//    $('.product-carousel-nav').slick({
//        slidesToShow: 3,
//        slidesToScroll: 1,
//        vertical: true,
//        asNavFor: '.product-carousel-main',
//        dots: false,
//        infinite: false,
//        focusOnSelect: true,
//        verticalSwiping: true
//    });

    $(".home-instagram-list").flickity({
        cellAlign: "left",
        autoPlay: false,
        wrapAround: false,
        contain: true,
        fade: false,
        groupCells: false,
        prevNextButtons: false,
        imagesLoaded: true,
        pageDots: false,
        watchCSS: false,
        freeScroll: true,
        on: {
            ready: function () {
                $(".testimonial-slider").addClass('ready');
            },
            change: function (index) {

            }
        }
    });

    $(".shop-slider").flickity({
        pageDots: false,
        prevNextButtons: false,
        imagesLoaded: true
    });

    $(".similar-product").flickity({
        cellAlign: "center",
        contain: true,
        wrapAround: false,
        autoPlay: false,
        fade: false,
        groupCells: true,
        prevNextButtons: true,
        imagesLoaded: true,
        pageDots: false,
        arrowShape: 'M362.671,490.787c-2.831,0.005-5.548-1.115-7.552-3.115L120.452,253.006c-4.164-4.165-4.164-10.917,0-15.083L355.119,3.256  c4.093-4.237,10.845-4.354,15.083-0.262c4.237,4.093,4.354,10.845,0.262,15.083c-0.086,0.089-0.173,0.176-0.262,0.262  L143.087,245.454l227.136,227.115c4.171,4.16,4.179,10.914,0.019,15.085C368.236,489.664,365.511,490.792,362.671,490.787z',
        on: {
            ready: function () {
                $(".similar-product").addClass('ready');
            },
            change: function (index) {

            }
        }
    });

    $(".extra-product").flickity({
        cellAlign: "center",
        contain: true,
        wrapAround: false,
        autoPlay: false,
        fade: false,
        groupCells: true,
        prevNextButtons: true,
        imagesLoaded: true,
        pageDots: false,
        arrowShape: 'M362.671,490.787c-2.831,0.005-5.548-1.115-7.552-3.115L120.452,253.006c-4.164-4.165-4.164-10.917,0-15.083L355.119,3.256  c4.093-4.237,10.845-4.354,15.083-0.262c4.237,4.093,4.354,10.845,0.262,15.083c-0.086,0.089-0.173,0.176-0.262,0.262  L143.087,245.454l227.136,227.115c4.171,4.16,4.179,10.914,0.019,15.085C368.236,489.664,365.511,490.792,362.671,490.787z',
        on: {
            ready: function () {
                $(".extra-product").addClass('ready');
            },
            change: function (index) {

            }
        }
    });


    $('.faq-box .faq-row').delegate('.title', 'click', function () {
        if (!$(this).hasClass('active')) {
            $('.faq-row > .title').removeClass('active');
            $('.faq-text').slideUp();
            $(this).addClass('active').parent().find('.faq-text').slideDown(function () {
                if ($(window).width() < 767) {
                    $('html, body').animate({
                        scrollTop: $('.faq-row > .title.active').offset().top - 85
                    }, 350);
                } else {
                    $('html, body').animate({
                        scrollTop: $('.faq-row > .title.active').offset().top - 160
                    }, 350);
                }
            });
        } else {
            $('.faq-text').slideUp();
            $('.faq-row > .title').removeClass('active');
        }
    });


    $('.header-right .search').click(function () {
        setTimeout(function () {
            $(".search-box .fields .field .form-control").focus();
        }, 1000);
        if ($('html').hasClass('search-open')) {
            $('html').removeClass('search-open');
            $('#s').focus();
        } else {
            $('html').addClass('search-open');
            $('#s').focus();
        }
    });


    $('.header-right .cart').click(function (event) {
        event.stopPropagation();
        event.preventDefault();
        if (!$('html').hasClass('minicart-open')) {
            $('html').addClass('minicart-open');
        } else {
            $('html').removeClass('minicart-open');
        }
        return false;
    });


    $(document).click(function (event) {
        event.stopPropagation();
        if ($('html').hasClass('minicart-open')) {
            $('html').removeClass('minicart-open');
        }
    });

    $('.minicart').click(function (event) {
        event.stopPropagation();
    });




    if ($("[data-fancybox]").length) {
        $("[data-fancybox]").fancybox({
            btnTpl: {
                smallBtn: '<div data-fancybox-close class="fancybox-close-small modal-close">Close <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 10.6L6.6 5.2 5.2 6.6l5.4 5.4-5.4 5.4 1.4 1.4 5.4-5.4 5.4 5.4 1.4-1.4-5.4-5.4 5.4-5.4-1.4-1.4-5.4 5.4z"></path></svg></div>'
            }
        });
    }


    $('.inner-banner .down-arrow ').click(function (e) {
        e.preventDefault();
        var hHeight = $('.header-page').outerHeight(true);

        if (window.innerWidth < 768) {
            $('html,body').animate({
                scrollTop: $('.inner-section').offset().top - 58
            }, 700);
        } else {
            $('html,body').animate({
                scrollTop: $('.inner-section').offset().top - 75
            }, 700);
        }
    });


    if ($('.fix-bar-section').length) {
        $('.fix-bar-section').fixTo('body', {
            className: 'my-class-name',
            zIndex: 10,
            top: 0
        });
    }

    if ($('.full-page-loader').length) {
        $('.full-page-loader').delay(500).fadeOut(200, function () {
            $('html').addClass('loader-loaded');
            $('.site-wrapper').animate({'opacity': '1'}, 200, function () {
                scrollContent();
                $(window).trigger('scroll');
                $('html').addClass('site-loaded');
            });
        });
    }

});


$(function () {

    scrollContent();
    doneResizing();


});

function doneResizing() {
    isResizing = true;
    if (window.innerWidth > 767) {
        if ($('.home-blog ul .flickity-viewport ').length) {
            $('.home-blog ul').flickity('destroy');
        }
    } else {
        if ($('.home-blog ul').length) {
            if ($('.home-blog ul  .flickity-viewport ').length) {
                $('.home-blog ul ').flickity('destroy');
            }
            $('.home-blog ul').flickity({
                cellAlign: 'center',
                contain: true,
                wrapAround: true,
                imagesLoaded: true,
                prevNextButtons: false,
                pageDots: false
            });
        }
    }


    if (window.innerWidth > 980) {
        if ($('.swipe-slider  .flickity-viewport ').length) {
            $('.swipe-slider').flickity('destroy');
        }

        if ($(".parpefullpage").length) {
            $(".parpefullpage").parpefullpage();
        }

    } else {
        if ($('.swipe-slider').length) {
            $('.slider-section , .slider-section div').removeAttr('style');
            if ($('.swipe-slider  .flickity-viewport ').length) {
                $('.swipe-slider ').flickity('destroy');
            }
            $('.swipe-slider').flickity({
                cellAlign: 'center',
                contain: true,
                fade: true,
                wrapAround: true,
                imagesLoaded: true,
                prevNextButtons: false,
                pageDots: true,
                on: {
                    ready: function () {
                        //countInSlider();
                    },
                    change: function (index) {
                        //countInSlider();
                    }
                }
            })


        }

    }


    isResizing = false;
}
var browserMobile = false;
if ($("body").hasClass("layout-mobile"))
    browserMobile = true;
$(window).scroll(function () {
    scrollContent();



});

var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = $('header').outerHeight();
$(window).scroll(function (event) {
    didScroll = true;
});
setInterval(function () {
    if (didScroll) {
        hasScrolled();
        didScroll = false;
    }
}, 250);

function hasScrolled() {
    var st = $(this).scrollTop();

    if (Math.abs(lastScrollTop - st) <= delta)
        return;

    if (st > lastScrollTop && st > navbarHeight) {
        $('header').removeClass('nav-down').addClass('nav-up');
        if ($('html').hasClass('minicart-open')) {
            $('html').removeClass('minicart-open');
        }
    } else {
        if (st + $(window).height() < $(document).height()) {
            $('header').removeClass('nav-up').addClass('nav-down');

        }
    }
    lastScrollTop = st;
}

function scrollContent() {
    var $toLoad = $(".to-load");
    var $toLoadElement = $(".to-load-element");
    if (browserMobile)
        $(".to-load").addClass("loaded");
    $(".to-load-element").addClass("loaded");
    if (browserMobile) {
        newScroll = $(window).scrollTop();
    } else {
        if (window.scrollY > 0) {
            newScroll = window.scrollY;
        } else {
            newScroll = $("html,body").scrollTop();
        }
    }
    if (!browserMobile) {
        $toLoad.each(function () {
            var object = $(this);
            if (newScroll + $(window).height() * 1.05 > $(this).offset().top) {
                object.removeClass("no-anim");
                object.addClass("loaded");

            } else if (newScroll + $(window).height() < $(this).offset().top) {
                object.addClass("no-anim");
                object.removeClass("loaded");

            }
        });
        $toLoadElement.each(function () {
            var object = $(this);
            if (newScroll + $(window).height() * 1.05 > $(this).offset().top) {
                object.removeClass("no-anim");
                object.addClass("loaded");

            } else if (newScroll + $(window).height() < $(this).offset().top) {
                object.addClass("no-anim");
                object.removeClass("loaded");

            }
        });
    } else {
        $(".to-load").addClass("loaded");
        $(".to-load-element").addClass("loaded");
    }
    currentScroll = newScroll;
}

function whichBrs() {
    var agt = navigator.userAgent.toLowerCase();
    if (agt.indexOf("opera") != -1)
        return "Opera";
    if (agt.indexOf("staroffice") != -1)
        return "Star Office";
    if (agt.indexOf("webtv") != -1)
        return "WebTV";
    if (agt.indexOf("beonex") != -1)
        return "Beonex";
    if (agt.indexOf("chimera") != -1)
        return "Chimera";
    if (agt.indexOf("netpositive") != -1)
        return "NetPositive";
    if (agt.indexOf("phoenix") != -1)
        return "Phoenix";
    if (agt.indexOf("firefox") != -1)
        return "Firefox";
    if (agt.indexOf("chrome") != -1)
        return "Chrome";
    if (agt.indexOf("safari") != -1)
        return "Safari";
    if (agt.indexOf("skipstone") != -1)
        return "SkipStone";
    if (agt.indexOf("msie") != -1)
        return "Internet Explorer";
    if (agt.indexOf("netscape") != -1)
        return "Netscape";
    if (agt.indexOf("mozilla/5.0") != -1)
        return "Mozilla";
    if (agt.indexOf("/") != -1) {
        if (agt.substr(0, agt.indexOf("/")) != "mozilla") {
            return navigator.userAgent.substr(0, agt.indexOf("/"));
        } else
            return "Netscape";
    } else if (agt.indexOf(" ") != -1)
        return navigator.userAgent.substr(0, agt.indexOf(" "));
    else
        return navigator.userAgent;
}

$(window).load(function () {
    stopAllVideos();
    $("html").addClass("dom-loaded site-loaded");
    doneResizing();


});


//check if any video is playing //

function stopAllVideos() {

    $('.video-box').each(function () {
        var targetVideo = $(this).find('iframe');
        $(this).find('iframe').remove();
        if (targetVideo.length) {
            var newVideoURL = targetVideo.prop('src');
            if (newVideoURL.indexOf("autoplay=1") != -1) {
                var replaceVideoURL = newVideoURL.replace("autoplay=1", "autoplay=0");
                var newIframe = '<iframe src = "' + replaceVideoURL + '" allow = "accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen  width = "850" height = "480"  frameborder = "0" > < /iframe>'
                // targetVideo.prop('src', replaceVideoURL);
                $(this).prepend(newIframe);
                $(this).removeClass('now-playing');

            } else {
                var replaceVideoURL = newVideoURL + "?autoplay=0&rel=0";
                var newIframe = '<iframe src = "' + replaceVideoURL + '" allow = "accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen  width = "850" height = "480"  frameborder = "0" > < /iframe>'
                $(this).prepend(newIframe);
                targetVideo.prop('src', replaceVideoURL);
            }
        }


    });

}



// discover scroll
(function ($) {
    $(window).on("load", function () {
        $(".discover-details, .search-box .home-search-wrapper ul").mCustomScrollbar();
    });
})(jQuery);


$(document).ajaxComplete(function (event, request, settings) {
    $('.c-select select').selectric({});
    $('.gfield select').selectric({});


});

$(window).load(function () {
    $(window).trigger('resize');
    $('html').addClass('dom-loaded');
    //$('.full-page-loader').addClass('start');

});