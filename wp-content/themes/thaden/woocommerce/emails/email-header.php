<?php
/**
 * Email Header
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-header.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates\Emails
 * @version 4.0.0
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="UTF-8">
        <link rel="icon" type="image/png" href="images/favicons/favicon-128.png" sizes="128x128" />
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700" rel="stylesheet">
        <title><?php echo get_bloginfo('name', 'display'); ?></title>
    </head>
    <style type="text/css">
            /*bugfix*/
            * {
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                -ms-box-sizing: border-box;
                box-sizing: border-box;
                -webkit-tap-highlight-color: rgba(255, 255, 255, 0) !important;
                -webkit-focus-ring-color: rgba(255, 255, 255, 0) !important;
                outline: 0 none;
            }
            *:after {
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                -ms-box-sizing: border-box;
                box-sizing: border-box;
            }
            *:before {
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                -ms-box-sizing: border-box;
                box-sizing: border-box;
            }
            #outlook a {
                padding: 0;
            }
            body {
                margin: 0 !important;
                padding: 0 !important;
                -webkit-text-size-adjust: 100% !important;
                -ms-text-size-adjust: 100% !important;
                -webkit-font-smoothing: antialiased !important;
            }
            img {
                border: 0 !important;
                outline: none !important;
                outline: none;
                text-decoration: none;
                -ms-interpolation-mode: bicubic;
            }
            a img {
                border: none;
            }
            p {
                margin: 0px !important;
                padding: 0px !important;
            }
            body {
                background-color: #F7F8F8;
            }
            /*mobile*/
        </style>
    </head>
    <body bgcolor="#F7F8F8" style="background:#F7F8F8">
        <table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" style="border:0; width: 640px; max-width: 640px; margin:0 auto; background-color:#FFFFFF; font-size:12px; line-height:18px; font-family: 'Montserrat', arial, sans-serif; border:1px solid #a0a0a0;" width="640" class="w360">
            <!--  HEADER START -->
            <tr>
                <td style="border-bottom: 1px solid #CBCBCB;">
                    <table border="0" cellpadding="0" cellspacing="0" width="640" class="w360">
                        <tr>
                            <td valign="top" height="15" align="center" style="height:15px; line-height:15px; background:#FFFFFF">&nbsp;</td>
                        </tr>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" width="640" class="w360" style="background:#FFFFFF">
                        <tr>
                            <td valign="top" width="640" class="w360" align="center">
                                <a href="<?php echo home_url('/');?>" target="_blank">
                                    <img src="<?php echo get_option('logo');?>" width="163" alt="Thaden Logo" style="vertical-align: top;">
                                </a>
                            </td>
                        </tr>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" width="640" class="w360">
                        <tr>
                            <td valign="top" height="15" align="center" style="height:15px; line-height:15px; background:#FFFFFF">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!--  HEADER START -->
           