<?php
/**
 * Customer processing order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-processing-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates\Emails
 * @version 3.7.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>
	<tr>
		<td>
			<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="text-align: center;">
				<tr>
					<td valign="top" height="30" align="center" style="height:30px; line-height:30px;">&nbsp;</td>
				</tr>
				<tr>
					<td align="center">
						<h2 style="font-family: 'Montserrat', arial, sans-serif; font-weight: 600; color:#000000!important; font-size:28px; line-height:38px; margin: 0; padding: 0; letter-spacing: 0.2px; text-transform: uppercase;">Thank you for PRE-order</h2>
					</td>
				</tr>
				<tr>
					<td valign="top" height="10" align="center" style="height:10px; line-height:10px;">&nbsp;</td>
				</tr>
				<tr>
					<td align="center">
						<p style="font-family: 'Montserrat', arial, sans-serif; font-weight: 400; color:#636363!important; font-size:14px; line-height:22px; margin: 0; padding: 0;">We make our leather products out of the finest,<br>genuine leather. </p>
					</td>
				</tr>
				<tr>
					<td valign="top" height="30" align="center" style="height:30px; line-height:30px;">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" cellpadding="0" cellspacing="0" width="640" class="w360">
				<tr>
					<td width="100%" align="center">
						<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
							<tr>
								<td width="100%" align="center" style="border-top: 1px solid #CBCBCB;"></td>
							</tr>
							<tr>
								<td valign="top" height="15" align="center" style="height:15px; line-height:15px;">&nbsp;</td>
							</tr>
							<tr>
								<td align="center">
									<h2 style="font-family: 'Montserrat', arial, sans-serif; font-weight: 400; color:#000000!important; font-size:18px; line-height:28px; margin: 0; padding: 0; letter-spacing: 0.2px; text-transform: uppercase;">pre-Order confirmation</h2>
								</td>
							</tr>
							<tr>
								<td valign="top" height="5" align="center" style="height:5px; line-height:5px;">&nbsp;</td>
							</tr>
							<tr>
								<td align="center">
									<p style="font-family: 'Montserrat', arial, sans-serif; font-weight: 400; color:#636363!important; font-size:14px; line-height:20px; margin: 0; padding: 0;">Order ID: #<?php echo $order->get_order_number();?></p>
								</td>
							</tr>
							<tr>
								<td align="center">
									<p style="font-family: 'Montserrat', arial, sans-serif; font-weight: 400; color:#636363!important; font-size:14px; line-height:20px; margin: 0; padding: 0;">Order Date: <?php echo $order->get_date_created();?></p>
								</td>
							</tr>
							<tr>
								<td valign="top" height="15" align="center" style="height:15px; line-height:15px;">&nbsp;</td>
							</tr>
							<tr>
								<td width="100%" align="center" style="border-bottom: 1px solid #CBCBCB;"></td>
							</tr>
							<tr>
								<td valign="top" height="30" align="center" style="height:30px; line-height:30px;">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<?php
			
				/*
				 * @hooked WC_Emails::customer_details() Shows customer details
				 * @hooked WC_Emails::email_address() Shows email address
				 */
				do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );
			?>
		</td>
	</tr>
	<tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="640" class="w360">
                        <tr>
                            <td width="100%" align="center">
                                <?php

								/*
								 * @hooked WC_Emails::order_details() Shows the order details table.
								 * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
								 * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
								 * @since 2.5.0
								 */
								do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );
								?>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" height="40" align="center" style="height:40px; line-height:40px;">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top" height="40" align="center" style="height:40px; line-height:40px;">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="640" class="w360">
                        <tr>
                            <td width="100%" align="center">
                                <table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
                                    <tr>
                                        <td width="100%" align="center" style="border-top: 1px solid #CBCBCB;"></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" height="20" align="center" style="height:20px; line-height:20px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center;">
                                                <tr>
                                                    <td width="47%" align="left" valign="top">&nbsp;</td>
                                                    <td width="6%" align="left" valign="top">&nbsp;</td>
                                                    <td width="47%" align="left" valign="top">
                                                       <?php
													   /*
														 * @hooked WC_Emails::order_meta() Shows order meta data.
														 */
														do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );
														?>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" height="20" align="center" style="height:20px; line-height:20px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="100%" align="center" style="border-bottom: 1px solid #CBCBCB;"></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" height="40" align="center" style="height:40px; line-height:40px;">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="border-radius:0px; background-color: #000000;" align="center">
                                <a href="<?php echo home_url('my-account/view-order/').$order->get_order_number();?>" target="_blank" style="font-family: 'Montserrat', arial, sans-serif; font-weight: 400; font-size: 16px; color: #FFFFFF; text-decoration: none; display: inline-block; text-transform: uppercase; border-top: 12px solid #000000; border-bottom: 12px solid #000000; border-right: 30px solid #000000; border-left: 30px solid #000000;">view order</a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top" height="40" align="center" style="height:40px; line-height:40px;">&nbsp;</td>
            </tr>

<?php

/*
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );
