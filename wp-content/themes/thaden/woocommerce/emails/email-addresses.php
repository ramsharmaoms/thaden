<?php
/**
 * Email Addresses
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-addresses.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates\Emails
 * @version 3.9.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$text_align = is_rtl() ? 'right' : 'left';
$address    = $order->get_formatted_billing_address();
$shipping   = $order->get_formatted_shipping_address();

?>
<table border="0" cellpadding="0" cellspacing="0" width="640" class="w360" id="addresses">
	<tr>
		<td width="100%" align="center">
			<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center" style="border-bottom: 1px solid #CBCBCB;">
				<tr>
					<td width="47%" align="left" valign="top">
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<h2 style="font-family: 'Montserrat', arial, sans-serif; font-weight: 700; color:#000000!important; font-size:18px; line-height:28px; margin: 0; padding: 0; letter-spacing: 0.2px; text-transform: uppercase;"><?php esc_html_e( 'Billing address', 'woocommerce' ); ?></h2>
								</td>
							</tr>
							<tr>
								<td valign="top" height="10" align="center" style="height:10px; line-height:10px;">&nbsp;</td>
							</tr>
							<tr>
								<td>
									<?php echo wp_kses_post( $address ? $address : esc_html__( 'N/A', 'woocommerce' ) ); ?>
									<?php if ( $order->get_billing_phone() ) : ?>
										<br/><?php echo wc_make_phone_clickable( $order->get_billing_phone() ); ?>
									<?php endif; ?>
									<?php if ( $order->get_billing_email() ) : ?>
										<br/><?php echo esc_html( $order->get_billing_email() ); ?>
									<?php endif; ?>
								</td>
							</tr>
						</table>
					</td>
					<td width="6%" align="left" valign="top">&nbsp;</td>
					<?php if ( ! wc_ship_to_billing_address_only() && $order->needs_shipping_address() && $shipping ) : ?>
					<td width="47%" align="left" valign="top">
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<h2 style="font-family: 'Montserrat', arial, sans-serif; font-weight: 700; color:#000000!important; font-size:18px; line-height:28px; margin: 0; padding: 0; letter-spacing: 0.2px; text-transform: uppercase;"><?php esc_html_e( 'Shipping address', 'woocommerce' ); ?></h2>
								</td>
							</tr>
							<tr>
								<td valign="top" height="10" align="center" style="height:10px; line-height:10px;">&nbsp;</td>
							</tr>
							<tr>
								<td>
									<?php echo wp_kses_post( $shipping ); ?>
								</td>
							</tr>
						</table>
					</td>
					<?php endif;?>
				</tr>
				<tr>
					<td valign="top" height="30" align="center" style="height:30px; line-height:30px;">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" height="40" align="center" style="height:40px; line-height:40px;">&nbsp;</td>
	</tr>
	</table>

