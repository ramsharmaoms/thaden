<?php
/**
 * Email Footer
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-footer.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates\Emails
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;
?>
				<!--  FOOTER START -->
			<tr>
                <td valign="top" height="20" align="center" style="height:20px; line-height:20px; border-top: 1px solid #CBCBCB; ">&nbsp;</td>
            </tr>
            <tr>
                <td width="100%" align="center">
                    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center;">
                        <tbody>
                            <tr>
                                <td width="30%" align="center">
                                    <a href="<?php echo home_url('contact/');?>" target="_blank" style="font-family: 'Montserrat', arial, sans-serif; font-weight: 600; color:#000000!important; font-size:18px; line-height:22px; margin: 0; padding: 0; text-decoration: none;">Contact</a>
                                </td>
                                <td width="30%" align="center">
                                    <a href="<?php echo home_url('shipping-returns/');?>" target="_blank" style="font-family: 'Montserrat', arial, sans-serif; font-weight: 600; color:#000000!important; font-size:18px; line-height:22px; margin: 0; padding: 0; text-decoration: none;">Shipping & Returns</a>
                                </td>
                                <td width="30%" align="center">
                                    <a href="<?php echo home_url('privacy/');?>" target="_blank" style="font-family: 'Montserrat', arial, sans-serif; font-weight: 600; color:#000000!important; font-size:18px; line-height:22px; margin: 0; padding: 0; text-decoration: none;">Privacy Policy</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top" height="20" align="center" style="height:20px; line-height:20px;">&nbsp;</td>
            </tr>
            <tr>
                <td width="100%" align="center" style="border-top: 1px solid #CBCBCB;">
                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td valign="top" height="30" align="center" style="height:30px; line-height:30px; background:#FFFFFF">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <a href="<?php echo esc_url( home_url());?>" target="_blank" style="font-family: 'Montserrat', arial, sans-serif; font-weight: 400; color:#000000!important; font-size:18px; line-height:22px; margin: 0; padding: 0; text-decoration: none;">www.thaden.ch</a>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" height="15" align="center" style="height:15px; line-height:15px; background:#FFFFFF">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="100%" align="center">
                                    <table width="180" border="0" cellpadding="0" cellspacing="0" align="center">
                                        <tbody>
                                            <tr>
                                                <td align="center" style="line-height:1px; font-size:1px;">
                                                    <a href="https://www.instagram.com/" target="_blank">
                                                        <img src="<?php echo get_template_directory_uri();?>/dist/images/email/instagram.png" alt="instagram" width="31" border="0" style="display:block; padding:0; margin:0; border:none;">
                                                    </a>
                                                </td>
                                                <td align="center" style="line-height:1px; font-size:1px;">
                                                    <a href="https://www.pinterest.de/Thadenofficial/" target="_blank">
                                                        <img src="<?php echo get_template_directory_uri();?>/dist/images/email/pinterest.png" alt="pinterest" width="24" border="0" style="display:block; padding:0; margin:0; border:none;">
                                                    </a>
                                                </td>
                                                <td align="center" style="line-height:1px; font-size:1px;">
                                                    <a href="https://www.linkedin.com/company/thadenluxurygoods/" target="_blank">
                                                        <img src="<?php echo get_template_directory_uri();?>/dist/images/email/linkedin.png" alt="linkedin" width="29" border="0" style="display:block; padding:0; margin:0; border:none;">
                                                    </a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" height="30" align="center" style="height:30px; line-height:30px; background:#FFFFFF">&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <!--  FOOTER END -->
        </table>
    </body>
</html>