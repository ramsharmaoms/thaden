<?php
/**
 * Order details table shown in emails.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-order-details.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates\Emails
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;

$text_align = is_rtl() ? 'right' : 'left';

do_action( 'woocommerce_email_before_order_table', $order, $sent_to_admin, $plain_text, $email ); ?>
<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center;">
	<tr>
		<td width="47%" align="left" valign="top">
			<img src="images/product-thumb-9.jpg" width="271" alt="Thumb" style="vertical-align: top;">
		</td>
		<td width="6%" align="left" valign="top">&nbsp;</td>
		<td width="47%" align="left" valign="top">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%">
						<h2 style="font-family: 'Montserrat', arial, sans-serif; font-weight: 700; color:#000000!important; font-size:18px; line-height:28px; margin: 0; padding: 0; letter-spacing: 0.2px; text-transform: capitalize;">The Little Rocket</h2>
					</td>
				</tr>
				<tr>
					<td valign="top" height="5" align="center" style="height:5px; line-height:5px;">&nbsp;</td>
				</tr>
				<tr>
					<td>
						<p style="font-family: 'Montserrat', arial, sans-serif; font-weight: 400; color:#636363!important; font-size:14px; line-height:20px; margin: 0; padding: 0;">Black</p>
					</td>
				</tr>
				<tr>
					<td valign="top" height="20" align="center" style="height:20px; line-height:20px; border-bottom: 1px solid #CBCBCB;">&nbsp;</td>
				</tr>
				<tr>
					<td valign="top" height="20" align="center" style="height:20px; line-height:20px;">&nbsp;</td>
				</tr>
				<tr>
					<td width="100%" align="center">
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="55%" valign="top">
									<p style="font-family: 'Montserrat', arial, sans-serif; font-weight: 400; color:#636363!important; font-size:12px; line-height:20px; margin: 0; padding: 0;">Quantity :</p>
								</td>
								<td width="45%" valign="top" align="right">
									<p style="font-family: 'Montserrat', arial, sans-serif; font-weight: 600; color:#000000!important; font-size:12px; line-height:20px; margin: 0; padding: 0;">1</p>
								</td>
							</tr>
							<tr>
								<td width="55%" valign="top" height="5" align="center" style="height:5px; line-height:5px;">&nbsp;</td>
								<td width="45%" valign="top" height="5" align="center" style="height:5px; line-height:5px;">&nbsp;</td>
							</tr>
							<tr>
								<td width="55%" valign="top">
									<p style="font-family: 'Montserrat', arial, sans-serif; font-weight: 400; color:#636363!important; font-size:12px; line-height:20px; margin: 0; padding: 0;">Total Price :</p>
								</td>
								<td width="45%" valign="top" align="right">
									<p style="font-family: 'Montserrat', arial, sans-serif; font-weight: 600; color:#000000!important; font-size:12px; line-height:20px; margin: 0; padding: 0;">$ 1,955.00</p>
								</td>
							</tr>
							<tr>
								<td width="55%" valign="top" height="5" align="center" style="height:5px; line-height:5px;">&nbsp;</td>
								<td width="45%" valign="top" height="5" align="center" style="height:5px; line-height:5px;">&nbsp;</td>
							</tr>
							<tr>
								<td width="55%" valign="top">
									<p style="font-family: 'Montserrat', arial, sans-serif; font-weight: 400; color:#636363!important; font-size:12px; line-height:20px; margin: 0; padding: 0;">Pre-payment 30% :</p>
								</td>
								<td width="45%" valign="top" align="right">
									<p style="font-family: 'Montserrat', arial, sans-serif; font-weight: 600; color:#000000!important; font-size:12px; line-height:20px; margin: 0; padding: 0;">$ 586.50.00</p>
								</td>
							</tr>
							<tr>
								<td width="55%" valign="top" height="5" align="center" style="height:5px; line-height:5px;">&nbsp;</td>
								<td width="45%" valign="top" height="5" align="center" style="height:5px; line-height:5px;">&nbsp;</td>
							</tr>
							<tr>
								<td width="55%" valign="top">
									<p style="font-family: 'Montserrat', arial, sans-serif; font-weight: 400; color:#636363!important; font-size:12px; line-height:20px; margin: 0; padding: 0;">Pending-payment 70% :</p>
								</td>
								<td width="45%" valign="top" align="right">
									<p style="font-family: 'Montserrat', arial, sans-serif; font-weight: 600; color:#000000!important; font-size:12px; line-height:20px; margin: 0; padding: 0;">$ 1,408.50.00</p>
								</td>
							</tr>
							<tr>
								<td width="55%" valign="top" height="5" align="center" style="height:5px; line-height:5px;">&nbsp;</td>
								<td width="45%" valign="top" height="5" align="center" style="height:5px; line-height:5px;">&nbsp;</td>
							</tr>
							<tr>
								<td width="55%" valign="top">
									<p style="font-family: 'Montserrat', arial, sans-serif; font-weight: 400; color:#636363!important; font-size:12px; line-height:20px; margin: 0; padding: 0;">Expected Delivery Date :</p>
								</td>
								<td width="45%" valign="top" align="right">
									<p style="font-family: 'Montserrat', arial, sans-serif; font-weight: 600; color:#000000!important; font-size:12px; line-height:20px; margin: 0; padding: 0;">November 2021</p>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<h2>
	<?php
	if ( $sent_to_admin ) {
		$before = '<a class="link" href="' . esc_url( $order->get_edit_order_url() ) . '">';
		$after  = '</a>';
	} else {
		$before = '';
		$after  = '';
	}
	/* translators: %s: Order ID. */
	echo wp_kses_post( $before . sprintf( __( '[Order #%s]', 'woocommerce' ) . $after . ' (<time datetime="%s">%s</time>)', $order->get_order_number(), $order->get_date_created()->format( 'c' ), wc_format_datetime( $order->get_date_created() ) ) );
	?>
</h2>

<div style="margin-bottom: 40px;">
	<table class="td" cellspacing="0" cellpadding="6" style="width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" border="1">
		<thead>
			<tr>
				<th class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Product', 'woocommerce' ); ?></th>
				<th class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Quantity', 'woocommerce' ); ?></th>
				<th class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Price', 'woocommerce' ); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php
			echo wc_get_email_order_items( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
				$order,
				array(
					'show_sku'      => $sent_to_admin,
					'show_image'    => false,
					'image_size'    => array( 32, 32 ),
					'plain_text'    => $plain_text,
					'sent_to_admin' => $sent_to_admin,
				)
			);
			?>
		</tbody>
		<tfoot>
			<?php
			$item_totals = $order->get_order_item_totals();

			if ( $item_totals ) {
				$i = 0;
				foreach ( $item_totals as $total ) {
					$i++;
					?>
					<tr>
						<th class="td" scope="row" colspan="2" style="text-align:<?php echo esc_attr( $text_align ); ?>; <?php echo ( 1 === $i ) ? 'border-top-width: 4px;' : ''; ?>"><?php echo wp_kses_post( $total['label'] ); ?></th>
						<td class="td" style="text-align:<?php echo esc_attr( $text_align ); ?>; <?php echo ( 1 === $i ) ? 'border-top-width: 4px;' : ''; ?>"><?php echo wp_kses_post( $total['value'] ); ?></td>
					</tr>
					<?php
				}
			}
			if ( $order->get_customer_note() ) {
				?>
				<tr>
					<th class="td" scope="row" colspan="2" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Note:', 'woocommerce' ); ?></th>
					<td class="td" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php echo wp_kses_post( nl2br( wptexturize( $order->get_customer_note() ) ) ); ?></td>
				</tr>
				<?php
			}
			?>
		</tfoot>
	</table>
</div>

<?php do_action( 'woocommerce_email_after_order_table', $order, $sent_to_admin, $plain_text, $email ); ?>
