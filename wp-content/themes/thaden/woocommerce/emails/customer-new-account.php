<?php
/**
 * Customer new account email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-new-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates\Emails
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<!--  BANNER START -->
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="640" class="w360" style="background:#FFFFFF">
                        <tr>
                            <td valign="top" width="640" class="w360" align="center">
                                <img src="<?php echo get_template_directory_uri();?>/dist/images/email/top-banner-welcome.jpg" width="640" height="302" border="0" alt="Banner" style="vertical-align: top;">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!--  BANNER END -->
            <!--  CONTENT START -->
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="640" class="w360">
                        <tr>
                            <td valign="top" height="50" align="center" style="height:50px; line-height:50px; background:#FFFFFF">&nbsp;</td>
                        </tr>
                    </table>
                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="center">
                                <h2 style="font-family: 'Montserrat', arial, sans-serif; font-weight: 700; color:#000000!important; font-size:28px; line-height:38px; margin: 0; padding: 0; letter-spacing: 0.2px;">WELCOME TO THADEN</h2>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" height="10" align="center" style="height:10px; line-height:10px; background:#FFFFFF">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">
                                <p style="font-family: 'Montserrat', arial, sans-serif; font-weight: 400; color:#636363!important; font-size:14px; line-height:22px; margin: 0; padding: 0;">Thank you for creating your account on the THADEN.<br>Now that you’re part of the Thaden Family, you’re in for a real treat!<br>We carry the latest styles from your favorite brand.</p>
                            </td>
                        </tr>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" width="640" class="w360">
                        <tr>
                            <td valign="top" height="15" align="center" style="height:15px; line-height:15px; background:#FFFFFF">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="640" class="w360">
                        <tr>
                            <td valign="top" height="15" align="center" style="height:15px; line-height:15px; background:#FFFFFF">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!--  BUTTON START -->
            <tr>
                <td align="center">
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="border-radius:0px; background-color: #000000;" align="center">
                                <a href="<?php echo home_url('shop/');?>" target="_blank" style="font-family: 'Montserrat', arial, sans-serif; font-weight: 400; font-size: 16px; color: #FFFFFF; text-decoration: none; display: inline-block; text-transform: uppercase; border-top: 12px solid #000000; border-bottom: 12px solid #000000; border-right: 20px solid #000000; border-left: 20px solid #000000;">SHOP THADEN.CH</a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!--  BUTTON END -->
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="640" class="w360">
                        <tr>
                            <td valign="top" height="50" align="center" style="height:50px; line-height:50px; background:#FFFFFF">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>


<?php /* translators: %s: Customer username  ?>
<p><?php printf( esc_html__( 'Hi %s,', 'woocommerce' ), esc_html( $user_login ) ); ?></p>
<?php /* translators: %1$s: Site title, %2$s: Username, %3$s: My account link  ?>
<p><?php printf( esc_html__( 'Thanks for creating an account on %1$s. Your username is %2$s. You can access your account area to view orders, change your password, and more at: %3$s', 'woocommerce' ), esc_html( $blogname ), '<strong>' . esc_html( $user_login ) . '</strong>', make_clickable( esc_url( wc_get_page_permalink( 'myaccount' ) ) ) ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
<?php if ( 'yes' === get_option( 'woocommerce_registration_generate_password' ) && $password_generated ) : ?>
	<?php /* translators: %s: Auto generated password  ?>
	<p><?php printf( esc_html__( 'Your password has been automatically generated: %s', 'woocommerce' ), '<strong>' . esc_html( $user_pass ) . '</strong>' ); ?></p>
<?php endif; ?>

<?php
/**
 * Show user-defined additional content - this is set in each email's settings.
 
if ( $additional_content ) {
	echo wp_kses_post( wpautop( wptexturize( $additional_content ) ) );
}
*/
do_action( 'woocommerce_email_footer', $email );
