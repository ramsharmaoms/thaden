<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */
defined('ABSPATH') || exit;

get_header('shop');
?>
<?php get_template_part('template-parts/herobanner/page-banner'); ?>

<section class="product-list-page fl-fix pos-r">


    <section class="fl-fix pos-r filter">
        <div class="full-wrapper">
            <div class="d-f fxw-w jc-sb ai-c filter-box">
                <div class="filter-view d-f ai-c">
                    <span class="d-ib va-t ff-Montserrat-Bold tt-u">view</span>
                    <ul class="filter-grid d-f ai-c">
                        <li class="active"><a data-view="grid-view-3" href="#" class="d-ib va-t view-link"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/grid-view-3.svg" alt="grid-view-3" width="22" height="22"></a></li>
                        <li><a data-view="grid-view-2" href="#" class="d-ib va-t view-link"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/grid-view-2.svg" alt="grid-view-2" width="16" height="16"></a></li>
                    </ul>
                </div>
                <?php
                global $wp_query;
                $cat_obj = $wp_query->get_queried_object();
                $excludecategory = array(432, 255);
                $all_categories = get_woocommerce_category($excludecategory, $parent = 0);
                if ($all_categories):
                    ?>
                    <div class="filter-category">
                        <ul class="d-f tt-u ai-c">
                            <li class="<?php echo (is_shop()) ? 'active' : ''; ?>"><a href="<?php echo home_url('shop/'); ?>">All</a></li>
                            <?php foreach ($all_categories as $cat) { ?>
                                <li class="<?php echo ($cat_obj->term_id == $cat->term_id) ? 'active' : ''; ?>"><a href="<?php echo get_term_link($cat->slug, 'product_cat'); ?>"><?php echo $cat->name; ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php endif; ?>
                <div class="filter-sort d-f ai-c">
                    <?php do_action('woocommerce_before_shop_loop'); ?>
<!--                    <span class="d-ib va-t tt-u">Sort</span>
                    <a href="#" class="d-ib va-t filter-sort-icon"><img src="<?php // echo get_template_directory_uri();     ?>/dist/images/sorting.svg" alt="sorting" width="15" height="11"></a>-->

                </div>
            </div>
        </div>
    </section>
    <!-- Filter Section-->
    <?php
    /**
     * Hook: woocommerce_before_main_content.
     *
     * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
     * @hooked woocommerce_breadcrumb - 20
     * @hooked WC_Structured_Data::generate_website_data() - 30
     */
    //do_action( 'woocommerce_before_main_content' );
    ?>

    <?php
    if (woocommerce_product_loop()) {

        /**
         * Hook: woocommerce_before_shop_loop.
         *
         * @hooked woocommerce_output_all_notices - 10
         * @hooked woocommerce_result_count - 20
         * @hooked woocommerce_catalog_ordering - 30
         */
        //do_action('woocommerce_before_shop_loop');

        woocommerce_product_loop_start();

        if (wc_get_loop_prop('total')) {
            while (have_posts()) {
                the_post();
                /**
                 * Hook: woocommerce_shop_loop.
                 */
                do_action('woocommerce_shop_loop');

                wc_get_template_part('content', 'product');
            }
        }

        woocommerce_product_loop_end();
        /**
         * Hook: woocommerce_after_shop_loop.
         *
         * @hooked woocommerce_pagination - 10
         */
        do_action('woocommerce_after_shop_loop');
    } else {
        /**
         * Hook: woocommerce_no_products_found.
         *
         * @hooked wc_no_products_found - 10
         */
        do_action('woocommerce_no_products_found');
    }

    /**
     * Hook: woocommerce_after_main_content.
     *
     * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
     */
//do_action( 'woocommerce_after_main_content' );
    ?>

</section>

<?php
get_footer('shop');
?>

<script>
    jQuery(document).ready(function () {
        jQuery('.orderby').on('change', function () {
            var orderby = jQuery(this).val();
            var reloadurl = '?orderby=' + orderby;
            window.location.href = reloadurl;
        })
    });
</script>