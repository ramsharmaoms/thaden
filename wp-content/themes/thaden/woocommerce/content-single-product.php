<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */
defined('ABSPATH') || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action('woocommerce_before_single_product');

if (post_password_required()) {
    echo get_the_password_form(); // WPCS: XSS ok.
    return;
}
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class('', $product); ?> >
    <section class="fl-fix pos-r product-detail">
        <div class="full-wrapper">
            <div class="d-f fxw-w jc-sb pos-r product-box w100">
                <?php
                /**
                 * Hook: woocommerce_before_single_product_summary.
                 *
                 * @hooked woocommerce_show_product_sale_flash - 10
                 * @hooked woocommerce_show_product_images - 20
                 */
                do_action('woocommerce_before_single_product_summary');
                ?>

                <div class="product-info col w45 padL50">
                    <div class="sticky-box">
                        <div class="product-text padT25">
                            <?php
                            /**
                             * Hook: woocommerce_single_product_summary.
                             *
                             * @hooked woocommerce_template_single_title - 5
                             * @hooked woocommerce_template_single_rating - 10
                             * @hooked woocommerce_template_single_price - 10
                             * @hooked woocommerce_template_single_excerpt - 20
                             * @hooked woocommerce_template_single_add_to_cart - 30
                             * @hooked woocommerce_template_single_meta - 40
                             * @hooked woocommerce_template_single_sharing - 50
                             * @hooked WC_Structured_Data::generate_product_data() - 60
                             */
                            do_action('woocommerce_single_product_summary');
                            ?>
                            <?php
                            $pre_payment_text = get_field('pre_payment_text');
                            if (!empty($pre_payment_text)):
                                ?> 
                                <div class="marT10">
                                    <?php echo apply_filters('the_content', $pre_payment_text); ?>
                                </div>	
                            <?php endif; ?>
                        </div>

                        <?php
                        //Product faqs section
                        $faqs = get_field('faqs');
                        if (!empty($faqs)):
                            ?>
                            <div class="faq-box">
                                <?php
                                $i = 1;
                                foreach ($faqs as $faq):
                                    ?>
                                    <div class="faq-row">
                                        <h4 class="h4 title pos-r"><?php echo $faq['faq_heading']; ?></h4>
                                        <div class="faq-text">
                                            <?php echo apply_filters('the_content', $faq['faq_text']); ?>
                                        </div>
                                    </div>	
                                    <?php
                                    $i++;
                                endforeach;
                                ?>							
                            </div>
                            <?php
                        endif;
                        //end faqs section
                        ?>
                        <?php if (!empty(get_field('products_videos'))): ?>
                            <div class="product-video-box">
                                <?php
                                $productvideos = get_field('products_videos');
                                foreach ($productvideos as $productvideo):
                                    ?>
                                    <?php if (!empty($productvideo['upload_video']['url'])): ?>
                                        <video autoplay muted loop class="video-tag">
                                            <source src="<?php echo $productvideo['upload_video']['url']; ?>" type="video/mp4">
                                        </video>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
    /**
     * Hook: woocommerce_after_single_product_summary.
     *
     * @hooked woocommerce_output_product_data_tabs - 10
     * @hooked woocommerce_upsell_display - 15
     * @hooked woocommerce_output_related_products - 20
     */
    do_action('woocommerce_after_single_product_summary');
    ?>
</div>

<?php do_action('woocommerce_after_single_product'); ?>
