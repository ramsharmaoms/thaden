<?php
/**
 * Simple product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/simple.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */
defined('ABSPATH') || exit;

global $product;

if (!$product->is_purchasable()) {
    return;
}

echo wc_get_stock_html($product); // WPCS: XSS ok.

if ($product->is_in_stock()) :
    ?>

    <?php do_action('woocommerce_before_add_to_cart_form'); ?>

    <form class="cart" action="<?php echo esc_url(apply_filters('woocommerce_add_to_cart_form_action', $product->get_permalink())); ?>" method="post" enctype='multipart/form-data'>
        <?php do_action('woocommerce_before_add_to_cart_button'); ?>
        <div class="product-qty">
            <label class="label w100">Qty :</label>
            <div class="qty-box pos-r d-ib va-t">
                <?php
                do_action('woocommerce_before_add_to_cart_quantity');

                woocommerce_quantity_input(
                        array(
                            'min_value' => apply_filters('woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product),
                            'max_value' => apply_filters('woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product),
                            'input_value' => isset($_POST['quantity']) ? wc_stock_amount(wp_unslash($_POST['quantity'])) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
                        )
                );
                do_action('woocommerce_after_add_to_cart_quantity');
                ?>
            </div>
        </div>
        <div class="product-price fs4 ff-Montserrat-Medium"><?php echo $product->get_price_html(); ?></div>
        <div class="marB10 yith-wcdp">
            <?php $deposit_value = apply_filters('yith_wcdp_deposist_value', min(YITH_WCDP_Premium()->get_deposit($product->get_id(), false, 'view'), $product->get_price()), $product); ?>
            <?php //echo wc_price($deposit_value);?>
            <?php
            // translators: 1. Deposit value.
            echo wp_kses_post(apply_filters('yith_wcdp_deposit_only_message', sprintf(__('This action will let you pay a deposit of <span class="deposit-price">%s</span> for this product.', 'yith-woocommerce-deposits-and-down-payments'), wc_price($deposit_value)), $deposit_value));
            ?>
        </div>
        <div class="action">
            <button type="submit" name="add-to-cart" value="<?php echo esc_attr($product->get_id()); ?>" class="single_add_to_cart_button  alt btn btn-cart"><?php echo esc_html($product->single_add_to_cart_text()); ?></button>
        </div>	

        <?php do_action('woocommerce_after_add_to_cart_button'); ?>
    </form>

    <?php do_action('woocommerce_after_add_to_cart_form'); ?>

<?php endif; ?>
<style>
    #yith-wcdp-add-deposit-to-cart { display:none;}
</style>