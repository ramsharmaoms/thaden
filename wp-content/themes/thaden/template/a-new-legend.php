<?php
/*
  Template Name: A New Legend
 */
get_header();
?>
<?php get_template_part('template-parts/herobanner/page-banner'); ?>
<section class="fl-fix pos-r about-pages padT60">
    <div class="full-wrapper">
        <?php $gallerysection = get_field('gallery_section'); ?>
        <?php if (!empty($gallerysection)): ?>
            <div class="thaden-grid fl-fix pos-r padTB60">
                <?php foreach ($gallerysection as $gallery): ?>
                    <figure class="bgsz-cv bgp-cc bgr-n figure" style="background-image: url(<?php echo $gallery['url']; ?>);"></figure>
                <?php endforeach; ?>	
            </div>
        <?php endif; ?>
        <div class="fl-fix content padTB40">
            <?php the_content(); ?>
        </div>
        <?php $bottomcontent = get_field('bottom_content'); ?>
        <?php if (!empty($bottomcontent)): ?>
            <div class="d-f fxw-w fl-fix jc-sb col-2-swap">
                <?php foreach ($bottomcontent as $content): ?>
                    <div class="d-f fxw-w jc-sb ai-c pos-r col-2-item w100">
                        <figure class="col w50 figure">
                            <img src="<?php echo $content['image']['url']; ?>" alt="<?php echo $content['image']['alt']; ?>" width="768" height="903" class="w100">
                        </figure>
                        <div class="col w50 caption">
                            <div class="wrapper">
                                <?php if (!empty($content['heading'])): ?>
                                    <h2 class="h1 title tt-u ff-Montserrat-Regular"><?php echo $content['heading']; ?></h2>
                                <?php endif; ?>	
                                <?php echo apply_filters('the_content', $content['description']); ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</section>
<!-- About THADEN Section-->		
<!-- About THADEN Section-->

<?php get_footer(); ?>
	  



