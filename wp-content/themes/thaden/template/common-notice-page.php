<?php
/*
  Template Name: Common Notice Page
 */
get_header('nogap');
?>
<section class="fl-fix pos-r page-title ta-c">
    <div class="wrapper">
        <h1 class="h1 title tt-u c-black"><?php echo get_the_title(); ?></h1>
        <?php the_content(); ?>
    </div>
</section>
<!-- Title Section-->
<section class="fl-fix pos-r content-grid">
    <div class="full-wrapper">
        <div class="wrapper">
            <div class="d-f fxw-w jc-sb">
                <div class="col w25">
                    <ul class="content-links fs4 ff-Montserrat-Medium">
                        <?php echo get_field('table_of_content'); ?>							
                    </ul>
                </div>
                <div class="col w70">
                    <?php echo apply_filters('the_content', get_field('page_content')); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>






