<?php
/*
  Template Name: Customer Experience
 */
get_header();
?>
<?php get_template_part('template-parts/herobanner/page-banner'); ?>
<section class="fl-fix pos-r about-pages padT60">
    <div class="full-wrapper">
        <div class="fl-fix content padTB40">

            <?php the_content(); ?>
        </div>
        <?php $customersimages = get_field('customers_images'); ?>
        <?php if (!empty($customersimages)): ?>
            <div class="customer-grid-3 d-f jc-sb fl-fix pos-r padTB60">
                <?php foreach ($customersimages as $customersimage): ?>
                    <figure class="bgsz-cv bgp-cc bgr-n figure" style="background-image: url(<?php echo $customersimage['url']; ?>);"></figure>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
        <?php
        $leftimage = get_field('left_image');
        $rightcontent = get_field('right_content');
        ?>
        <?php if (!empty($leftimage) || !empty($rightcontent)): ?>
            <div class="d-f fxw-w jc-sb ai-c pos-r fl-fix customer-grid padTB40">
                <figure class="col <?php if ($rightcontent): ?> w40 <?php endif; ?> figure">
                    <img src="<?php echo $leftimage['url']; ?>" alt="<?php echo $leftimage['alt']; ?>" width="666" height="767" class="w100">
                </figure>
                <div class="col <?php if ($leftimage): ?> w55 <?php endif; ?> caption">
                    <div class="wrapper">
                        <?php echo apply_filters('the_content', $rightcontent); ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>	
        <?php
        $leftimage = get_field('full_width_content');
        $bottombanner = get_field('bottom_banner');
        ?>
        <?php if (!empty(get_field('full_width_content')) || !empty($bottombanner)): ?>
            <div class="fl-fix content padTB40">				
                <?php echo apply_filters('the_content', get_field('full_width_content')); ?>
                <?php if (!empty($bottombanner)): ?>
                    <figure class="figure padT40">
                        <img src="<?php echo $bottombanner['url']; ?>" alt="life-grid" width="1668" height="700" class="w100">
                    </figure>
                <?php endif; ?>
            </div>
        <?php endif; ?>
        <?php if (!empty(get_field('bottom_content'))): ?>
            <div class="fl-fix content padTB40">
                <?php echo apply_filters('the_content', get_field('bottom_content')); ?>
            </div>
        <?php endif; ?>	
    </div>
</section>			
<!-- About THADEN Section-->

<?php get_footer(); ?>
	  



