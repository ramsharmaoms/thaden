<?php
/*
  Template Name: value of Fashion
 */
get_header();
?>
<?php get_template_part('template-parts/herobanner/page-banner'); ?>
<section class="fl-fix pos-r about-pages padT60">
    <div class="full-wrapper">
        <?php $bottomcontent = get_field('bottom_content'); ?>
        <?php if (!empty($bottomcontent)): ?>
            <div class="d-f fxw-w fl-fix jc-sb col-2-swap">
                <?php foreach ($bottomcontent as $content): ?>
                    <div class="d-f fxw-w jc-sb ai-c pos-r col-2-item w100" id="<?php echo strtolower(str_replace(' ','-',$content['heading']));?>">
                        <figure class="col w50 figure">
                            <img src="<?php echo $content['image']['url']; ?>" alt="<?php echo $content['image']['alt']; ?>" width="768" height="903" class="w100">
                        </figure>
                        <div class="col w50 caption">
                            <div class="wrapper">
                                <?php if (!empty($content['heading'])): ?>
                                    <h2 class="h1 title tt-u ff-Montserrat-Regular"><?php echo $content['heading']; ?></h2>
                                <?php endif; ?>	
                                <?php echo apply_filters('the_content', $content['description']); ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
        <?php $customerexperience = get_field('customer_experience'); ?>
        <?php /*if (!empty($customerexperience)): ?>
            <div class="d-f fxw-w fl-fix jc-sb col-2-swap" id="customer-experience">
                <div class="d-f fxw-w jc-sb ai-c pos-r col-2-item w100">
                    <figure class="col w50 figure">
                        <img src="<?php echo get_template_directory_uri(); ?>/dist/images/customer-experience.jpg" alt="customer-experience" width="768" height="903" class="w100">
                    </figure>
                    <div class="col w50 caption">
                        <div class="wrapper">
                            <h2 class="h1 title tt-u ff-Montserrat-Regular">CUSTOMER EXPERIENCE</h2>
                            <p>Thaden wants to be in touch with you. Be it through a personal meeting in our showrooms in Berne/Switzerland or Milano/Italy, on Instagram, Pinterest or at one of our events. Or meet the designer in person in a video call. Hear the exciting stories behind our products, learn more about Thaden’s values and become part of the community. Because we believe that a great client experience should be personalised, mindful and tailored to the needs of an experienced, savvy client.</p>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif;*/ ?>
    </div>
</section>
<!-- About THADEN Section-->		
<!-- About THADEN Section-->

<?php get_footer(); ?>
	  



