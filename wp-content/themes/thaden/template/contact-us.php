<?php
/*
  Template Name: Contact Us
 */
get_header('nogap');
?>
<section class="fl-fix pos-r page-title ta-c">
    <div class="wrapper">

        <?php the_content(); ?>

    </div>
</section>
<!-- Title Section-->
<section class="fl-fix pos-r contact-box">
    <div class="wrapper">
        <div class="d-f fxw-w jc-sb">
            <?php if (!empty(get_field('left_content'))): ?>
                <div class="col w50 ta-c">

                    <div class="box ta-l d-ib va-t">
                        <h3 class="h3 ff-Montserrat-Bold title c-black-2 padB5"><?php echo get_field('left_heading'); ?></h3>
                        <?php $leftcontents = get_field('left_content'); ?>
                        <?php foreach ($leftcontents as $leftcontent): ?>
                            <div class="contact-row marB40">
                                <h5 class="h5 ff-Montserrat-Bold title c-black-2 marB10"><?php echo $leftcontent['lft_heading']; ?></h5>
                                <div class="h6 title-icon"><?php echo apply_filters('the_content', $leftcontent['left_content']); ?></div>
                            </div>
                        <?php endforeach; ?>	
                    </div>
                </div>
            <?php endif; ?>
            <?php if (!empty(get_field('right_content'))): ?>
                <div class="col w50 ta-c">
                    <div class="box ta-l d-ib va-t">
                        <h3 class="h3 ff-Montserrat-Bold title c-black-2 padB5"><?php echo get_field('right_heading'); ?></h3>
                        <?php $rightcontents = get_field('right_content'); ?>
                        <?php foreach ($rightcontents as $rightcontent): ?>
                            <div class="contact-row marB40">
                                <h5 class="h5 ff-Montserrat-Bold title c-black-2 marB10"><?php echo $rightcontent['rgt_heading']; ?></h5>
                                <div class="h6 title-icon"><?php echo apply_filters('the_content', $rightcontent['rgt_content']); ?></div>
                            </div>
                        <?php endforeach; ?>
                        <?php if (!empty(get_field('ph_text'))): ?>
                            <div class="contact-row marB40">
                                <h5 class="h5 ff-Montserrat-Bold title c-black-2 marB10"><?php echo get_field('ph_heading'); ?></h5>
                                <div class="h6 title-icon"><a href="tel:<?php echo get_field('ph_text'); ?>" class="d-f ai-c"><img src="<?php echo get_field('ph_icon')['url']; ?>" alt="Phone Icon" width="26" height="26" class="marR25"><?php echo get_field('ph_text'); ?></a></div>
                            </div>
                        <?php endif; ?>

                        <?php if (!empty(get_field('follow_link'))): ?>
                            <div class="contact-row padT20">
                                <h5 class="h5 ff-Montserrat-Bold title c-black-2 marB10"><?php echo get_field('follow_heading'); ?></h5>
                                <?php $followlinks = get_field('follow_link'); ?>
                                <ul class="social-icons d-f ai-c c-black">
                                    <?php foreach ($followlinks as $followlink): ?>
                                        <li><a href="<?php echo $followlink['follow_link']; ?>" target="_blank"><i class="<?php echo $followlink['class']; ?>"></i></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>	
        </div>
    </div>
</section>

<?php get_footer(); ?>






