<?php
/*
  Template Name: Homepage
 */
get_header();
?>
<?php get_template_part('template-parts/herobanner/home-banner'); ?>
<?php
$categories = get_terms(array('taxonomy' => 'product_cat', 'parent' => '0', 'hide_empty' => false));
if (!empty($categories)) {
    ?>
    <section class="fl-fix pos-r home-category">
        <div class="full-wrapper">
            <div class="d-f fxw-w jc-sb home-category-list">
                <?php
                foreach ($categories as $category) {
                    $categoryID = $category->term_id;
                    $thumbnail_id = (int) get_woocommerce_term_meta($categoryID, 'thumbnail_id', true);
                    $cat_img = wp_get_attachment_url($thumbnail_id);
                    ?>
                    <div class="home-category-item">
                        <figure class="figure pos-r">
                            <a href="<?php echo get_term_link($category); ?>"><img src="<?php echo $cat_img; ?>" alt="category-image" width="814" height="1008" class="w100"></a>
                            <figcaption class="caption pos-a tt-u c-black">
                                <h3 class="h1"><?php echo $category->name; ?></h3>
                                <a href="<?php echo get_term_link($category); ?>" class="link fs4 d-ib va-t">shop NOW</a>
                            </figcaption>
                        </figure>
                    </div>
                <?php } ?>			
            </div>
        </div>
    </section>
<?php } ?>
<!-- Home Category Section-->
<?php
$items = get_field('items');
if (!empty($items)) {
    ?>		
    <section class="fl-fix pos-r home-grid">
        <?php
        foreach ($items as $item) {
            $itemImage = $item['item_image'];
            $itemTitle = $item['item_title'];
            $itemsubTitle = $item['item_subtitle'];
            $itemContent = $item['item_content'];
            $itemButton = $item['item_button_name'];
            $itemButtonurl = $item['item_button_url'];
            if (!empty($itemImage)) {
                ?>
                <div class="d-f fxw-w jc-sb ai-c home-grid-item ov-h">
                    <figure class="figure pos-r w50">
                        <img src="<?php echo $itemImage['url']; ?>" alt="grid-image" width="960" height="1080" class="w100">
                    </figure>
                    <div class="home-grid-text pos-r w50">
                        <div class="home-grid-center">
                            <div class="wrapper">
                                <div class="caption w50">
                                    <div class="caption-box">
                                        <?php if (!empty($itemTitle)) { ?><span class="tag d-b fs4 ff-Montserrat-Medium tt-u marB20"><?php echo $itemTitle; ?></span><?php } ?>
                                        <?php if (!empty($itemsubTitle)) { ?><h2 class="h1 title ff-Montserrat-Regular tt-u c-black-2"><?php echo $itemsubTitle; ?></h2><?php } ?>
                                        <?php
                                        if (!empty($itemContent)) {
                                            echo $itemContent;
                                        }
                                        ?>
                                        <?php if (!empty($itemButton)) { ?><a href="<?php echo $itemButtonurl; ?>" class="btn"><?php echo $itemButton; ?></a><?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
        }
        ?>		
    </section>
<?php } ?>	

<!-- Home Grid Section-->
<section class="fl-fix pos-r home-instagram">
    <?php //echo do_shortcode('[instagram_feed title=yes] ');?>
    <?php if (is_active_sidebar('instagram-feed')) { ?>
        <?php dynamic_sidebar('instagram-feed'); ?>
    <?php } ?>
    <?php //echo do_shortcode('[do_widget id=wpzoom_instagram_widget-3]');?>
</section>

<?php get_footer(); ?>
	  



