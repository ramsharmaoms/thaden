<?php
/*
  Template Name: FAQs Page
 */
get_header('nogap');
?>
<section class="fl-fix pos-r page-title ta-c">
    <div class="wrapper">
        <h1 class="h1 title tt-u c-black"><?php echo get_the_title(); ?></h1>
        <?php the_content(); ?>
    </div>
</section>
<!-- Title Section-->
<section class="fl-fix pos-r content-grid">
    <div class="full-wrapper">
        <div class="wrapper">
            <div class="d-f fxw-w jc-sb">
                <div class="col w25">					
                    <h3 class="h3 title c-black padB35">Table of Contents</h3>
                    <?php
                    $taxonomy = 'thaden_faq';
                    //$number = 2; // number of terms to display per page
                    $countet = 1;
                    $args = array(
                        'orderby' => 'name',
                        'order' => 'ASC',
                        'hide_empty' => true,
                        //'number' => $number,
                        'fields' => 'all',
                        'hierarchical' => true,
                        'child_of' => 0,
                        'pad_counts' => false,
                        //'offset' => $offset,
                        'cache_domain' => 'core'
                    );

                    $terms = get_terms($taxonomy, $args);
                    ?>
                    <?php if (!empty($terms)): ?>
                        <ul class="content-links fs4 ff-Montserrat-Medium">
                            <?php
                            $i = 1;
                            foreach ($terms as $term):if ($i == 1): $cateid = $term->term_id;
                                endif;
                                ?>
                                <li <?php if ($i == 1): ?>class="active"<?php endif; ?>><a href="javascript:void(0);" class="load_faq" data-id = <?php echo $term->term_id; ?>><?php echo $term->name; ?></a></li>
                                <?php
                                $i++;
                            endforeach;
                            ?>							
                        </ul>
                    <?php endif; ?>
                </div>
                <div class="col w70">
                    <div class="faq-box faqs_list">
                        <?php
                        $args = array('post_type' => 'faqs', 'post_status' => 'publish', 'posts_per_page' => -1, 'tax_query' => array(
                                array(
                                    'taxonomy' => 'thaden_faq',
                                    'field' => 'term_id',
                                    'terms' => $cateid,
                                ),));
                        $query = new WP_Query($args);
                        $i = 0;
                        if ($query->have_posts()):
                            ?>
                            <?php while ($query->have_posts()) : $query->the_post(); ?>
                                <div class="faq-row ">
                                    <h5 class="h5 title pos-r"><?php echo get_the_title(); ?></h5>
                                    <div class="faq-text">
                                        <?php echo apply_filters('the_content', $query->post->post_content); ?>
                                    </div>
                                </div>
                            <?php endwhile; ?>				 
                            <?php
                        endif;
                        wp_reset_query();
                        ?>
                    </div>	
                </div>
            </div>
        </div>
    </div>	
</section>
<script>
    jQuery(document).on('click', '.load_faq', function () {
        var faqcat = jQuery(this).data('id');
        $('.load_faq').parent('li').removeClass('active');
        $.ajax({
            url: "<?php echo site_url() ?>/wp-admin/admin-ajax.php",
            type: 'post',
            data: {
                action: 'get_thaden_faqs',
                category: faqcat
            },
            success: function (response) {
                $(this).parent('li').addClass('active');
                $('.faqs_list').html(response);
            }
        });
    })

</script>
<?php get_footer(); ?>






