<?php
/*
  Template Name: About Thaden
 */
get_header();
?>
<?php get_template_part('template-parts/herobanner/page-banner'); ?>
<?php $aboutthadens = get_field('content_block'); ?>
<?php if (!empty($aboutthadens)): ?>
    <section class="fl-fix pos-r about-pages padT60">
        <div class="full-wrapper">
            <div class="fl-fix d-f fxw-w jc-sb col-2-swap">
                <?php foreach ($aboutthadens as $aboutthaden): ?>
                    <div class="d-f fxw-w jc-sb ai-c pos-r col-2-item w100">
                        <figure class="col w50 figure">
                            <img src="<?php echo $aboutthaden['image']['url']; ?>" alt="<?php echo $aboutthaden['image']['alt']; ?>" width="768" height="903" class="w100">
                        </figure>
                        <div class="col w50 caption">
                            <div class="wrapper">
                                <?php if (!empty($aboutthaden['heading'])): ?>
                                    <h2 class="h1 title tt-u ff-Montserrat-Regular c-black-2"><?php echo $aboutthaden['heading']; ?></h2>
                                <?php endif; ?>	
                                <?php if (!empty($aboutthaden['description'])): ?>
                                    <?php echo apply_filters('the_content', $aboutthaden['description']); ?>
                                <?php endif; ?>	
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
<?php endif; ?>
<!-- About THADEN Section-->

<?php get_footer(); ?>
	  



