<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage thaden
 * @since thaden 1.0
 */
?>
</div>


<div class="footer-page bg-white">
    <div class="footer-top">
        <div class="wrapper">
            <div class="d-f jc-sb">
                <div class="footer-logo">
                    <a href="<?php echo home_url('/'); ?>" class="logo d-ib va-t"><img src="<?php echo get_option('footer_logo'); ?>" alt="Thaden Logo" width="159" height="169"></a>
                </div>
                <div class="footer-menu">
                    <h5 class="h5 title">Info</h5>
                    <?php
                    wp_nav_menu(array(
                        'menu' => 'Footer Menu',
                        'container' => 'ul',
                        'menu_class' => 'footer-links'
                    ));
                    ?>
                </div>
                <div class="footer-menu">
                    <h5 class="h5 title">Customer Service</h5>
                    <?php
                    wp_nav_menu(array(
                        'menu' => 'Customer Care',
                        'container' => 'ul',
                        'menu_class' => 'footer-links'
                    ));
                    ?>
                </div>
                <div class="newsletter">
                    <div class="newsletter-box"> 
                        <?php echo get_option('footer_subscribe_text'); ?>                                      
                        <?php echo do_shortcode(get_option('gravity_right')); ?>                                       
                    </div>
                    <ul class="social-icons d-f ai-c c-black">
                        <li><a href="<?php echo get_option('instagram_url'); ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
                        <?php if (!empty(get_option('facebook_url'))): ?>
                            <li><a href="<?php echo get_option('facebook_url'); ?>" target="_blank"><i class="fa fa-facebook-f"></i></a></li>
                        <?php endif; ?>
                        <?php if (!empty(get_option('pinterest_url'))): ?>
                            <li><a href="<?php echo get_option('pinterest_url'); ?>" target="_blank"><i class="fa fa-pinterest-p"></i></a></li>
                        <?php endif; ?>
                        <?php if (!empty(get_option('linkedin_url'))): ?>
                            <li><a href="<?php echo get_option('linkedin_url'); ?>" target="_blank"><i class="fa fa-linkedin-in"></i></a></li>
                        <?php endif; ?>
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom ta-c">
        <span><?php echo "Copyright@THADEN" . " " . date('Y') . ". All Rights Reserved " ?></span>
    </div>
</div>

<!-- Footer Section -->
<!--<div class="newsletter-popup" id="newsletter-popup" style="display: none;">
    <div class="wrapper">
        <div class="d-f fxw-w jc-sb pos-r ai-c">
            <figure class="figure pos-r">
                <img src="<?php echo get_option('footer_image'); ?>" alt="newsletter-image" width="360" height="451" >
            </figure>
            <div class="newsletter">
                <div class="newsletter-box">
<?php echo get_option('popup_subscribe_text'); ?>
<?php echo do_shortcode(get_option('popup_subscribe_form')); ?>
                </div>
            </div>
        </div>
    </div>
</div>-->


<!-- Newsletter Pop Up Section -->
</div>
<!-- Main Section -->

<?php wp_footer(); ?>

</body>
<!-- Body End-->
</html>
<!-- HTML End-->
