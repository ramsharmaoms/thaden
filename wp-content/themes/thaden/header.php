<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage thaden
 * @since thaden 1.0
 */
?><!DOCTYPE HTML>
<html lang="en">
    <head>
        <!-- Site Title -->
       
        <!-- Meta -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <!-- FavIcons -->
        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo get_template_directory_uri();?>/dist/images/favicons/apple-touch-icon-57x57.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_template_directory_uri();?>/dist/images/favicons/apple-touch-icon-114x114.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_template_directory_uri();?>/dist/images/favicons/apple-touch-icon-72x72.png" />
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_template_directory_uri();?>/dist/images/favicons/apple-touch-icon-144x144.png" />
        <link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo get_template_directory_uri();?>/dist/images/favicons/apple-touch-icon-60x60.png" />
        <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo get_template_directory_uri();?>/dist/images/favicons/apple-touch-icon-120x120.png" />
        <link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo get_template_directory_uri();?>/dist/images/favicons/apple-touch-icon-76x76.png" />
        <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo get_template_directory_uri();?>/dist/images/favicons/apple-touch-icon-152x152.png" />
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri();?>/dist/images/favicons/favicon-196x196.png" sizes="196x196" />
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri();?>/dist/images/favicons/favicon-96x96.png" sizes="96x96" />
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri();?>/dist/images/favicons/favicon-32x32.png" sizes="32x32" />
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri();?>/dist/images/favicons/avicon-16x16.png" sizes="16x16" />
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri();?>/dist/images/favicons/favicon-128.png" sizes="128x128" />
        <meta name="application-name" content="&nbsp;"/>
        <meta name="msapplication-TileColor" content="#FFFFFF" />
        <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri();?>/dist/images/favicons/mstile-144x144.png" />
        <meta name="msapplication-square70x70logo" content="<?php echo get_template_directory_uri();?>/dist/images/favicons/mstile-70x70.png" />
        <meta name="msapplication-square150x150logo" content="<?php echo get_template_directory_uri();?>/dist/images/favicons/mstile-150x150.png" />
        <meta name="msapplication-wide310x150logo" content="<?php echo get_template_directory_uri();?>/dist/images/favicons/mstile-310x150.png" />
        <meta name="msapplication-square310x310logo" content="<?php echo get_template_directory_uri();?>/dist/images/favicons/mstile-310x310.png" />
       
        <?php wp_head(); ?>
        
        <?php echo get_option('google_analytics'); ?>
    </head>
        
    <!-- Body -->
    <body <?php if(is_front_page()){ body_class('home-page'); } else { body_class(); }?>>
        <div class="full-page-loader">
            <div class="wrapper">
                <div class="image-box">
                    <img class="transition" src="<?php echo get_template_directory_uri(); ?>/dist/images/loader.gif" alt="Castle Hot Tubs">
                </div>
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>
        </div>
        <div class="site-wrapper fl-fix">
            <?php get_template_part( 'template-parts/header/header-main' ); ?>
            <!-- Header Section-->

            <div class="main fl-fix bg-background">
