<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @subpackage Plugin
 */
class WOOMULTI_CURRENCY_Exim_Import_CSV {

	protected static $instance = null;

	public static function instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}


	private function __construct() {
		add_action( 'wp_ajax_wmc_bulk_fixed_price', array( $this, 'import_csv' ) );
	}

	public function import_csv() {

		$ext = explode( '.', $_FILES['csv_file']['name'] );
		$pos = sanitize_text_field( $_POST['pos'] );
		$row = sanitize_text_field( $_POST['row'] );

		if ( in_array( $_FILES['csv_file']['type'], array(
				'text/csv',
				'application/vnd.ms-excel'
			) ) && end( $ext ) == 'csv' ) {
			if ( ( $file_data = fopen( $_FILES['csv_file']['tmp_name'], "r" ) ) !== false ) {
				$size   = ( $_FILES['csv_file']['size'] );
				$header = fgetcsv( $file_data );

				if ( $pos == 0 ) {
					$pos = ftell( $file_data );
				}

				fseek( $file_data, $pos );

				for ( $i = 0; $i < 30; $i ++ ) {
					$data = fgetcsv( $file_data );
					if ( ! empty( $data ) ) {

						$reg        = $sale = array();
						$id         = $data[0];
						$src        = array_combine( $header, $data );
						$currencies = $this->get_active_currencies();

						foreach ( $currencies as $currency ) {
							$reg[ $currency ]  = isset( $src[ $currency ] ) ? $src[ $currency ] : '';
							$sale[ $currency ] = isset( $src[ $currency . '-sale' ] ) ? $src[ $currency . '-sale' ] : '';
						}

						update_post_meta( $id, '_regular_price_wmcp', json_encode( $reg ) );
						update_post_meta( $id, '_sale_price_wmcp', json_encode( $sale ) );
						$row ++;
					}
				}

				$current_pos = ftell( $file_data );

				$percentage = round( $current_pos / $size * 100 );

				$data = array( 'pos' => $current_pos, 'percentage' => $percentage, 'row' => $row );

				wp_send_json_success( $data );
			} else {
				wp_send_json_error( array( 'message' => esc_html__( 'Unable to read file', 'woocommerce-multi-currency' ) ) );
			}
		} else {
			wp_send_json_error( array( 'message' => esc_html__( 'File not supported', 'woocommerce-multi-currency' ) ) );
		}
	}

	public function get_active_currencies() {
		return get_option( 'woo_multi_currency_params' )['currency'];
	}
}
