<?php 
/* 
Plugin Name: Setting panel	
Version: 1.0.1
Description: Add extra options into theme
Author: Appnova
Author URI: http://www.wordpress.org
$layout = get_option('theme_layout');
*/

/* Add menu page callback funtion */
function theme_settings_page(){

    ?>
	    <div class="wrap">
	    <h1>Theme Panel</h1>
	    <form method="post" action="options.php" enctype="multipart/form-data">
	        <?php	          
	            do_settings_sections("theme-options");   
				settings_fields("section1");
				settings_fields("section");				
	            submit_button(); 
	        ?>          
	    </form>
		</div>
	<?php
}



/* Display Facebook Field */
function display_facebook_element()
{
	?>
    	<input type="text" name="facebook_url" id="facebook_url" value="<?php echo get_option('facebook_url'); ?>" size="50" />
    <?php
}

/* Display Facebook Field */
function display_pinterest_element()
{
	?>
    	<input type="text" name="pinterest_url" id="pinterest_url" value="<?php echo get_option('pinterest_url'); ?>" size="50" />
    <?php
}

/* Display Linkedin Field */
function display_instagram_element()
{
	?>
    	<input type="text" name="instagram_url" id="instagram_url" value="<?php echo get_option('instagram_url'); ?>" size="50"/>
    <?php
}
/* Display Linkedin Field */
function display_linkedin_element()
{
	?>
    	<input type="text" name="linkedin_url" id="linkedin_url" value="<?php echo get_option('linkedin_url'); ?>" size="50"/>
    <?php
}

/* Display Google Analytics Code Field */
function display_google_element()
{
	?>
		<textarea name="google_analytics" id="google_analytics" rows=5 cols=48><?php echo get_option('google_analytics'); ?></textarea>
    	
    <?php
}


/* Display Google Analytics Code Field */
function display_header_offer_element()
{
	?>
		<textarea name="header_offer" id="header_offer" rows=5 cols=48><?php echo get_option('header_offer'); ?></textarea>
    	
    <?php
}

/* Display Google Analytics Code Field */
function display_flyout_offer_element()
{
	?>
		<textarea name="flyout_offer" id="flyout_offer" rows=5 cols=48><?php echo get_option('flyout_offer'); ?></textarea>
    	
    <?php
}

/* Display Google Analytics Code Field */
function display_cartitem_offer_element()
{
	?>
		<textarea name="cartitem_offer" id="cartitem_offer" rows=5 cols=48><?php echo get_option('cartitem_offer'); ?></textarea>
    	
    <?php
}

/* Display Google Analytics Code Field */
function display_cartshipping_offer_element()
{
	?>
		<textarea name="cartshipping_offer" id="cartshipping_offer" rows=5 cols=48><?php echo get_option('cartshipping_offer'); ?></textarea>
    	
    <?php
}

/* Display partners section Field */
function display_partners_section_element()
{
	$content = get_option('partners_section');
	wp_editor(
		$content,
		'partners_section',
		array(
		  'media_buttons' => true,
		  'textarea_rows' => 8,
		  'tabindex' => 4,
		  'tinymce' => array(
			'theme_advanced_buttons1' => 'bold, italic, ul, pH, temp',
		  ),
		)
	);
	
}


/* Display Contact Sales Url Field */
function display_email_element()
{
	?>
    	<input type="text" name="email_address" id="email_address" value="<?php echo get_option('email_address'); ?>" size="50" />
    <?php
}


/* Display Contact Support Url Field */
function display_phone_number_element()
{
	?>
    	<input type="text" name="phone_number" id="phone_number" value="<?php echo get_option('phone_number'); ?>" size="50" />
    <?php
}


/* Display Qognify Locations Url Field */
function display_footer_address_element()
{
	?>
    	<textarea name="footer_subscribe_text" id="footer_subscribe_text" rows=5 cols=48><?php echo get_option('footer_subscribe_text'); ?></textarea>
    <?php
}

/* Display Qognify Locations Url Field */
function display_popup_text_element()
{
	?>
    	<textarea name="popup_subscribe_text" id="popup_subscribe_text" rows=5 cols=48><?php echo get_option('popup_subscribe_text'); ?></textarea>
    <?php
}

/* Display Logo Field for Header */
function logo_display()
{
	?>
         <input type="file" name="logo" />
		<?php if(get_option('logo')): ?>
			<div><img src="<?php echo get_option('logo'); ?>" width="200" height="200"/></div>
			
		<?php endif;?>
	<?php
}



/* Upload Media files */
function handle_logo_upload()
{
	if(!empty($_FILES["logo"]["tmp_name"]))
	{	
		
		$urls = wp_handle_upload($_FILES["logo"], array('test_form' => FALSE));
	    $temp = $urls["url"];
		return $temp;  
		
	}
	 else{
		$temp = get_option('logo');
		return $temp;
	}	 
	return $option;
}


/* Display Logo Field for Header */
function mobile_logo_display()
{ 
	?>
		<input type="file" name="mobile_logo" />
		<?php if(get_option('mobile_logo')): ?>
			<div><img src="<?php echo get_option('mobile_logo'); ?>" width="200" height="200"/></div>			
		<?php endif;?>
	<?php
}


/* Display Logo Field for Header */
function global_footer_image_display()
{ 
	?>
		<input type="file" name="footer_image" />
		<?php if(get_option('footer_image')): ?>
			<div><img src="<?php echo get_option('footer_image'); ?>" width="200" height="200"/></div>			
		<?php endif;?>
	<?php
}
function display_gravity_element()
{
	?>
		<input type="text" name="gravity_right" id="gravity_right" value="<?php echo get_option('gravity_right'); ?>" size="50"/>    	
    <?php
}

function display_popup_gravity_element()
{
	?>
		<input type="text" name="popup_subscribe_form" id="popup_subscribe_form" value="<?php echo get_option('popup_subscribe_form'); ?>" size="50"/>    	
    <?php
}

/* Display Logo Field for Footer */
function footer_logo_display()
{
	?>
         <input type="file" name="footer_logo" />
		<?php if(get_option('footer_logo')): ?>
			<div><img src="<?php echo get_option('footer_logo'); ?>" width="200" height="200"/></div>
			
		<?php endif;?>
	<?php
}
/* Upload Media files */
function handle_mobile_logo_upload()
{	
	if(!empty($_FILES["mobile_logo"]["tmp_name"]))
	{			
		$urls = wp_handle_upload($_FILES["mobile_logo"], array('test_form' => FALSE));
	    $temp = $urls["url"];
		return $temp;  		
	}
	 else{
		$temp = get_option('mobile_logo');
		return $temp;
	}	 
	return $option;
}

/* Upload Media files */
function handle_footer_logo_upload()
{	
	if(!empty($_FILES["footer_logo"]["tmp_name"]))
	{			
		$urls = wp_handle_upload($_FILES["footer_logo"], array('test_form' => FALSE));
	    $temp = $urls["url"];
		return $temp;  		
	}
	 else{
		$temp = get_option('footer_logo');
		return $temp;
	}	 
	return $option;
}

/* Upload Media files */
function handle_footer_image_upload()
{	
	if(!empty($_FILES["footer_image"]["tmp_name"]))
	{			
		$urls = wp_handle_upload($_FILES["footer_image"], array('test_form' => FALSE));
	    $temp = $urls["url"];
		return $temp;  		
	}
	 else{
		$temp = get_option('footer_image');
		return $temp;
	}	 
	return $option;
}



/* Save all field on the page */
function display_theme_panel_fields()
{	
	add_settings_section("section", "All Settings", null, "theme-options");	
	add_settings_field("instagram_url", "Instagram Profile Url", "display_instagram_element", "theme-options", "section");	
	add_settings_field("facebook_url", "Facebook Profile Url", "display_facebook_element", "theme-options", "section");
	add_settings_field("pinterest_url", "Pinterest Profile Url", "display_pinterest_element", "theme-options", "section");
	add_settings_field("linkedin_url", "Linkedin Profile Url", "display_linkedin_element", "theme-options", "section");
	add_settings_field("google_analytics", "Google Analytics Code", "display_google_element", "theme-options", "section");
	//add_settings_field("header_offer", "Header Offer", "display_header_offer_element", "theme-options", "section");
	//add_settings_field("flyout_offer", "Flyout Offer", "display_flyout_offer_element", "theme-options", "section");
	//add_settings_field("cartitem_offer", "Cart Items Offer", "display_cartitem_offer_element", "theme-options", "section");
	
	//add_settings_field("cartshipping_offer", "Cart Shipping Offer", "display_cartshipping_offer_element", "theme-options", "section");
	//add_settings_field("email_address", "Email Addres", "display_email_element", "theme-options", "section");
	//add_settings_field("phone_number", "Contact Phone Number", "display_phone_number_element", "theme-options", "section");
	
	//add_settings_field("partners_section", "Footer Partners Section", "display_partners_section_element", "theme-options", "section");
	add_settings_field("logo", "Header Logo", "logo_display", "theme-options", "section");	
	//add_settings_field("mobile_logo", "Footer Logo", "mobile_logo_display", "theme-options", "section");	
	
    add_settings_field("gravity_right", "Footer Subscribe Form", "display_gravity_element", "theme-options", "section");
	add_settings_field("footer_subscribe_text", "Footer Subscribe Text", "display_footer_address_element", "theme-options", "section");
	add_settings_field("footer_image", "Popup Subscribe Image", "global_footer_image_display", "theme-options", "section");	
	 add_settings_field("popup_subscribe_form", "Popup Subscribe Form", "display_popup_gravity_element", "theme-options", "section");
	add_settings_field("popup_subscribe_text", "Footer Subscribe Text", "display_popup_text_element", "theme-options", "section");
    add_settings_field("footer_logo", "Footer Logo", "footer_logo_display", "theme-options", "section");	

	register_setting("section", "logo", "handle_logo_upload");
	//register_setting("section", "mobile_logo", "handle_mobile_logo_upload");
	
	
	register_setting("section", "facebook_url");
	register_setting("section", "pinterest_url");
	register_setting("section", "linkedin_url");
	register_setting("section", "google_analytics");
	//register_setting("section", "header_offer");
	//register_setting("section", "flyout_offer");
	//register_setting("section", "cartitem_offer");
	//register_setting("section", "partners_section");
	//register_setting("section", "cartshipping_offer");
	//register_setting("section", "email_address");
	//register_setting("section", "phone_number");
	
	register_setting("section", "instagram_url");
	register_setting("section", "footer_subscribe_text");	
	register_setting("section", "gravity_right");
	register_setting("section", "footer_image", "handle_footer_image_upload");
	register_setting("section", "popup_subscribe_form");
	register_setting("section", "popup_subscribe_text");
	register_setting("section", "footer_logo", "handle_footer_logo_upload");
	
    
	?>

<?php 
}

add_action("admin_init", "display_theme_panel_fields");

/* Add menu page on left panel*/
function add_theme_menu_item()
{
	add_options_page("Theme Options", "Theme Options", "manage_options", "theme-options", "theme_settings_page", null, 80);
}

add_action("admin_menu", "add_theme_menu_item");

// Add settings link on plugin page
function settings_link($links) { 
  $settings_link = '<a href="admin.php?page=theme-options">Settings</a>'; 
  array_unshift($links, $settings_link); 
  return $links; 
}
 
$plugin = plugin_basename(__FILE__); 
add_filter("plugin_action_links_$plugin", 'settings_link' );
?>
