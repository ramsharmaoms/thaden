<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'thaden' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ';5zIcD>,If=dfMuu.plI#)RV&<t{ WM_$yLhMHSEcaZ$Qur;gd^-Ud^=HY`D*lwm' );
define( 'SECURE_AUTH_KEY',  'V52ch55qTXWb~-7%m0JvVJ#kf4sb[k|c_]JA]{(uXgo$)@_TZQn-,m>8hg9KInia' );
define( 'LOGGED_IN_KEY',    'Ag;^P[^3n0UKlE6O-_4#NsklHN.+q%@floY+Jj7]{;#;qmKd]t=TjU/X^lN+-ET7' );
define( 'NONCE_KEY',        'V meqZ]UmbVaU<?I]O8D52`36}&IaPI8x3g`B5fP)p%QxR^n8}$V?iI@~`2yLDF]' );
define( 'AUTH_SALT',        'fSKhuE+&m<>BkO[F3$ju&oYsp529j+xn8W|CAeJ{`5#5DF.Z7)*{faW$[AS?`s~v' );
define( 'SECURE_AUTH_SALT', '4C)lU5A}`IS+@x0pOI_%jQ7ZKsCIc@?fxUi`.%JTp;HGS&KTXVOv]1l|yQ]FnD>9' );
define( 'LOGGED_IN_SALT',   '~yVdq/?4e;KwWhLNO{$$+>/SxUy4Y?&M^_DVOt>4MNe0E!MwL^Sonp=D^pBI|cr>' );
define( 'NONCE_SALT',       'jSz7re3i/QzB;Xh:W,5n.KYMs3E$_mVr*qj`iB/+9ZS Bz*OBAoD|v+pWVuM,+7?' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'tha_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
